((() => {

    'use strict';

    angular.module('app')
        .directive('quickFilter', () => ({
            templateUrl: 'filters/filter.html',
            scope: {
                items: '<',
                exportToFile: '=',
                mode: '=',
                alreadySelected: '<',
            },
            controller: 'FilterController',
            link: (scope, element) => {
                element.bind("keydown keypress", event => {
                    if (event.which === 13) {
                        scope.applyFilter();
                        event.preventDefault();
                    }
                });
            }
        }));

})());