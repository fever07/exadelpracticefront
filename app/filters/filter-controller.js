(() => {

    'use strict';

    angular.module('app')
        .controller('FilterController', FilterController);

    FilterController.$inject = ['$scope', 'staticService'];

    function FilterController($scope, staticService) {

        function init() {
            $scope.control = {};
            $scope.selected = {};
            $scope.alreadySelected = $scope.alreadySelected || {};
            $scope.toggleText = 'Show more options';
            $scope.selected.skills = [];
            $scope.selected = Object.assign({}, $scope.selected, $scope.alreadySelected);
            $scope.showAdvanced = false;
        }

        init();

        function createFilterConfig() {
            const filterConfig = {};
            Object.keys($scope.selected).forEach(item => {
                if ($scope.selected[item]) {
                    filterConfig[item] = $scope.selected[item]
                }
            });
            return filterConfig;
        }

        $scope.applyFilter = () => {
            $scope.$emit('quickSearch:applyFilter', createFilterConfig());
        };

        $scope.skillTreeCallback = staticService.getSkillsTree;

        $scope.goAdvanced = () => {
            $scope.showAdvanced = !$scope.showAdvanced;
            $scope.toggleText = $scope.showAdvanced ? 'Hide more options' : 'Show more options';
        };

        $scope.$on('quickSearch:clear', () => {
            $scope.selected = {};
        });

        $scope.clear = () => {
            $scope.selected = {};
            $scope.applyFilter();
        };

        $scope.fileExport = () => {

            $scope.exportToFile(createFilterConfig());
        }
    }
})();

