((() => {
    'use strict';
    angular.module('app')
        .directive('stars', () => ({
            scope: {
                count: '<'
            },
            template: '<i ng-repeat="i in getNumber(count) track by $index" class="mdi mdi-star mdi-24px color-yellow"></i>',
            controller: $scope => {
                $scope.getNumber = num => new Array(num)
            }
        }))
})());
