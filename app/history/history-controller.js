((() => {
    'use strict';
    angular.module('app')
        .controller('HistoryController', HistoryController);

    HistoryController.$inject = ['$scope', 'id', 'loadCallback'];

    function HistoryController($scope, id, loadCallback) {

        $scope.history = [];

        $scope.query = {
            skip: 0,
            amount: 10
        };

        $scope.showMore = true;

        $scope.loadMore = () => {

            $scope.showMore = false;
            loadCallback(id, $scope.query)
                .then(response => {
                    if (response.length) {
                        $scope.showMore = true;
                    }
                    $scope.query.skip += $scope.query.amount;
                    $scope.history = $scope.history.concat(response);
                })
        };

        $scope.loadMore();

    }

})());
