(() => {
    'use strict';
    angular.module('app')
        .filter('historyIcons', () => {
            return (item) => {
                const icons = {
                    'created': 'mdi-plus',
                    'update': 'mdi-pencil',
                };

                let icon = 'mdi-help';

                if (item) {
                    Object.keys(icons).some(key => {
                        if (item.toLowerCase().includes(key)) {
                            icon = icons[key];
                            return true;
                        }
                    })
                }
                return icon;
            }
        })

})();
