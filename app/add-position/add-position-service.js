(function () {
    
    'use strict';
    
    angular.module('app').factory('addPositionService', addPositionService);
    
    addPositionService.$inject = [
        'appConstants',
        'staticService'
    ];
    
    function addPositionService(appConstants, staticService) {
        
        const dateFields = [
            'requestDate',
            'projectDate',
        ];
        
        const numericFields = [
            'experience',
            'level',
        ];
        
        function positionParser(key, value) {
            if (dateFields.indexOf(key) !== -1)
                return new Date(value);
            if (numericFields.indexOf(key) !== -1)
                return parseInt(value);
            return value;
        }
        
        function setPositionToLocal(newPosition) {
            localStorage.setItem('add-position', JSON.stringify(newPosition));
        }
        
        function getPositionFromLocal() {
            const position = localStorage.getItem('add-position');
            return position ? JSON.parse(position, positionParser) : {};
        }
        
        let statusesList;
        staticService.getPositionStatuses()
            .then(response => {
                statusesList = response;
            });
        
        function convertPosition(positionToConvert) {
            
            const position = JSON.parse(JSON.stringify(positionToConvert), positionParser);
            position.skills = position.skills.map(skill => ({
                id: skill,
                isPrimary: false,
            }));
            position.skills.unshift({
                id: position.primarySkill,
                isPrimary: true,
            });
            
            //If no status chosen default status set
            if (!position.status)
                position.status = {
                    id: statusesList.filter(status => status.name === appConstants.DEFAULT_POSITION_STATUS)[0].id,
                };
            
            delete position.primarySkill;
            delete position.additionalSkills;
            delete position.counterSelects;
            
            return position;
            
        }
        
        function deconvertPosition(position) {
            
            position.primarySkill = position.skills.find(skill => skill.isPrimary).id;
            position.counterSelects = position.skills.length - 1;
            position.additionalSkills = [];
            
            position.skills = position.skills
                .filter(skill => !skill.isPrimary)
                .map(skill => skill.id);
            
            return position;
            
        }
        
        return {
            
            getPositionFromLocal,
            setPositionToLocal,
            convertPosition,
            deconvertPosition,
            
        };
        
    }
    
}());
