(function () {
    
    'use strict';
    
    angular.module('app').config(config);
    
    config.$inject = [
        '$stateProvider',
    ];
    
    function config($stateProvider) {
        
        $stateProvider.state({
            name: 'add-position',
            url: '/add-position',
            templateUrl: 'add-position/add-position.html',
            controller: 'AddPositionController as addposition',
            data: {
                authorizedRoles: ['admin', 'hrm']
            }
        }).state({
            name: 'edit-position',
            url: '/editposition/:id',
            templateUrl: 'add-position/add-position.html',
            controller: 'AddPositionController as addposition',
            params: {
                id: null,
            },
            data: {
                authorizedRoles: ['admin', 'hrm']
            }
        });
    }
    
})();
