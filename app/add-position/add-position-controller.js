(function () {
    
    'use strict';
    
    angular.module('app').controller('AddPositionController', AddPositionController);
    
    AddPositionController.$inject = [
        '$scope',
        '$state',
        '$stateParams',
        'addPositionService',
        'positionApiService',
        'staticService'
    ];
    
    function AddPositionController($scope, $state, $stateParams, addPositionService, positionApiService, staticService) {
        
        const state = $state.current.name;
        
        if (state === 'edit-position') {
            positionApiService.getPositionDetails($stateParams.id)
                .then(response => {
                    this.position = addPositionService.deconvertPosition(response);
                }, error => {
                    console.log(error);
                });
        } else
            this.position = addPositionService.getPositionFromLocal(state);
        
        this.skillsTreeRequest = staticService.getSkillsTree;
        
        staticService.getCities().then(res => {
            this.citiesList = res;
        });
        
        staticService.getProfessions().then(res => {
            this.professionsList = res;
        });
        
        staticService.getPositionStatuses().then(res => {
            this.statusesList = res;
        });
        
        staticService.getProjects().then(res => {
            this.projectsList = res;
        });
        
        this.addAdditionalSkill = function () {
            
            if (!this.position.additionalSkills) {
                this.position.additionalSkills = [];
                this.position.counterSelects = 0;
            }
            
            this.position.additionalSkills.push({
                index: this.position.counterSelects++,
                chosen: '',
                chosenLevel: '',
                chosenYear: null,
                chosenExp: null,
            });
            
        };
        
        this.removeAdditionalSkill = function () {
            
            this.position.additionalSkills.splice(this.position.additionalSkills.length - 1, 1);
            
        };
        
        this.submit = false;
        
        this.submitCompletely = function (id) {
            this.submit = true;
            localStorage.removeItem(state);
            $state.go('position', {id: id});
        };
        
        this.addPosition = function () {
            this.triedToSubmit = true;
            if ($scope.positionForm.$valid)
                if (state === 'add-position') {
                    this.sendingPosition = true;
                    positionApiService.postPosition(addPositionService.convertPosition(this.position))
                        .then(response => {
                            this.sendingPosition = false;
                            this.submitCompletely(response.data.id);
                        }, error => {
                            this.sendingPosition = false;
                            this.serverError = error;
                        });
                } else {
                    const id = this.position.id;
                    this.sendingPosition = true;
                    positionApiService.putPosition(id, addPositionService.convertPosition(this.position))
                        .then(response => {
                            this.sendingPosition = false;
                            this.submitCompletely(id);
                        }, error => {
                            this.sendingPosition = false;
                            this.serverError = error;
                        });
                }
        };
        
        const _this = this;
        
        $scope.$on('$destroy', function () {
            if (!_this.submit)
                addPositionService.setPositionToLocal(_this.position);
        });
        
        window.onbeforeunload = function () {
            if (!_this.submit)
                addPositionService.setPositionToLocal(_this.position);
        };
        
    }
    
}());
