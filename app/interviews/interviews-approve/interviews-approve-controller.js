
((() => {
    'use strict';
    angular.module('app')
        .controller('InterviewsApproveController', InterviewsApproveController);

    InterviewsApproveController.$inject = [
        '$scope',
        '$state',
        '$mdDialog',
        'interviewsService',
        'id'
    ];

    function InterviewsApproveController($scope, $state, $mdDialog, interviewsService, id) {
        interviewsService.getInterview(id).then(response => {
            $scope.interview = response;
            if ($scope.interview.status.indexOf("waiting")!==-1){
                $scope.wasConfirmed = false;
            } else{
                $scope.wasConfirmed = true;
            }
        })
            .catch(error => {
                $state.go('error', {error: error});
            })
            .finally(() => {
                $scope.isLoading = false;
            });

        $scope.approvingVerdict="Approve";



        $scope.putInterview = function () {
                $scope.interview.status =interviewsService.statusManager(
                $scope.interview.type,
                $scope.interview.feedbackMessage,
                $scope.approvingVerdict
            );




            if($scope.approvingVerdict == "Approve") {
                $scope.skillsForPatch = $scope.interview.skills.map(item => {
                    return {
                        id: item.skillId,
                        rating: item.confRating,
                        wasConfirmed: true,
                        experience: item.experience,
                        isPrimary: item.isPrimary,
                        name: item.skillName,
                    }
                });
                interviewsService.patchSkills($scope.interview, $scope.skillsForPatch);
            }
            interviewsService.putInterview($scope.interview.id, $scope.interview);
            $mdDialog.cancel();
        };

        $scope.candidateDetails = function () {
            $state.go('candidate', {id: $scope.interview.candidate.id});
            $mdDialog.cancel();
        };

        $scope.getAccess = function () {
            return localStorage.getItem('userRole');
        };

    }
})());