(function () {

    'use strict';

    angular.module('app')
        .factory('interviewsService', interviewsService);

    interviewsService.$inject = ['$http', 'appConstants'];

    function interviewsService($http, appConstants) {
        const service = {};
        service.sendInterview = function (interview) {
            const request = {
                method: 'POST',
                url: `${appConstants.apiUrl}/interviews`,
                data: interview,
                withCredentials: true,
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${localStorage.getItem('token')}`,
                }
            };
            return $http(request).then(response => {
            });
        };

        service.getInterview = function (id) {
            const request = {
                method: 'GET',
                url: `${appConstants.apiUrl}/interviews/${id}`,
                withCredentials: true,
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${localStorage.getItem('token')}`,
                }
            };
            return $http(request).then(response => {
                return response.data;
            });
        };


        service.putInterview = function (id, interview) {
            const request = {
                method: 'PUT',
                url: `${appConstants.apiUrl}/interviews/${id}`,
                data: interview,
                withCredentials: true,
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${localStorage.getItem('token')}`,
                }
            };
            return $http(request).then(response => {
            });
        };

        service.statusManager = function (typeInterview, feedbackStatus, approvingVerdict) {
            typeInterview = typeInterview || "";
            feedbackStatus = feedbackStatus  || "";
            approvingVerdict = approvingVerdict || "";
            if (feedbackStatus && typeInterview === "ts" && !approvingVerdict) {
                feedbackStatus = feedbackStatus + "ed" + " waiting approving from HR"
            }

            if (approvingVerdict) {
                feedbackStatus = feedbackStatus + "ed";
                approvingVerdict = ", HR " + approvingVerdict +"d";
            }
            console.log(typeInterview);
            return "Interview " + typeInterview.toUpperCase().toString() + " " + feedbackStatus.toString() + approvingVerdict;
        };

        service.patchSkills = function (interview, skills) {
            const request = {
                method: 'PATCH',
                url: `${appConstants.apiUrl}/candidates/skills/${interview.candidate.id}`,
                data: { interviewId: interview.id, skills: skills },
                withCredentials: true,
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${localStorage.getItem('token')}`,
                }
            };
            console.log(interview.candidate.id);
            console.log(JSON.stringify(request.data));
            return $http(request).then(response => {
            });
        };
        return service;

    }
}());