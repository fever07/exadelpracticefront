
((() => {
    'use strict';
    angular.module('app')
        .controller('InterviewsAddController', InterviewsAddController);

    InterviewsAddController.$inject = [
        '$scope',
        '$mdDialog',
        'interviewsService',
        'loginService',
        'positionApiService',
        'candidate'
    ];

    function InterviewsAddController($scope, $mdDialog, interviewsService, loginService, positionApiService, candidate) {
        $scope.interview = {
            type: "ts",
            candidate: {
                id: ""
            },
            header: "",
            interviewerEmail: "",
            hrmEmail: localStorage.getItem('userEmail'),
            englishLevel: 0,
            date: "",
            skills: [],
            position: "",
            description: ""
        };
        $scope.date="";
        $scope.time="";

        $scope.selected = [];
        $scope.candidate = candidate;
        $scope.skills = candidate.skills;
        $scope.interview.candidate.id = candidate.id;


        $scope.toggle = function (item, list) {
            let idx = list.indexOf(item);
            if (idx > -1) {
                list.splice(idx, 1);
            }
            else {
                list.push(item);
            }
        };

        $scope.exists = function (item, list) {
            return list.indexOf(item) > -1;
        };

        $scope.isIndeterminate = function () {
            return ($scope.selected.length !== 0 &&
            $scope.selected.length !== $scope.skills.length);
        };

        $scope.isChecked = function () {
            return $scope.selected.length === $scope.skills.length;
        };

        $scope.toggleAll = function () {
            if ($scope.selected.length === $scope.skills.length) {
                $scope.selected = [];
            } else if ($scope.selected.length === 0 || $scope.selected.length > 0) {
                $scope.selected = $scope.skills.slice(0);
            }
        };

        loginService.getHrms().then(response => {
            $scope.hrmsSpec = response;
        });

        loginService.getTechs().then(response => {
            $scope.techsSpec = response;
        });

        positionApiService.getPositions("active").then(response => {
            $scope.positions = response.positions;
        });

        $scope.hide = function () {
            $mdDialog.hide();
        };


        $scope.cancel = function () {
            $mdDialog.cancel();
        };

        $scope.answer = function (answer) {
            $mdDialog.hide(answer);
        };


        $scope.sendInterview = function () {
            $scope.interview.skills = $scope.selected.map(item => {
                return {
                    skillId: item.id,
                    defultRating: item.rating,
                    confRating: 0,
                    wasConfirmed: false,
                    experience: item.experience,
                    isPrimary: item.isPrimary,
                    skillName: item.name
                }
            });
            const dateDate = new Date(`${$scope.date}`);
            const dateTime = new Date(`${$scope.time}`);
            dateDate.setHours(dateTime.getHours());
            dateDate.setMinutes(dateTime.getMinutes());

            $scope.interview.date = dateDate;
            $scope.interview.status = interviewsService.statusManager($scope.interview.type);
            interviewsService.sendInterview($scope.interview);
            $mdDialog.cancel();
        }
    }
})());