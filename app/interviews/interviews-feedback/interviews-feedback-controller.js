((() => {

    'use strict';
    angular.module('app')
        .controller('InterviewsFeedbackController', InterviewsFeedbackController);

    InterviewsFeedbackController.$inject = [
        '$scope',
        '$state',
        '$mdDialog',
        '$mdToast',
        'interviewsService',
        'staticService',
        'id',
    ];

    function InterviewsFeedbackController($scope, $state, $mdDialog, $mdToast, interviewsService, staticService, id) {
        interviewsService.getInterview(id).then(response => {
            $scope.interview = response;
            $scope.interview.feedbackMessage = "Approve";
            if (response.type === "ts") {
                $scope.interview.feedbackMessage = "Recommend";
            }

            $scope.wasConfirmed = false;
            if($scope.interview.status.length > 15){
                $scope.wasConfirmed = true;
            }

        }).catch(error => {
            $mdToast.show($mdToast.simple()
                .textContent(`${error.statusText}`)
                .position('bottom right')
                .toastClass('vladloh'));
        })
            .finally(() => {
                $scope.isLoading = false;
            })
        ;

        $scope.skillsTreeRequest = staticService.getSkillsTree;

        $scope.printSkills = function () {
            console.log($scope.additionalSkills);
        };


        $scope.englishRanks = [
            { key: "disabled", val: 0 },
            { key: "A1", val: 1 },
            { key: "A2", val: 2 },
            { key: "B1", val: 3 },
            { key: "B2", val: 4 },
            { key: "C1", val: 5 },
            { key: "C2", val: 6 },
            { key: "Native", val: 7 }
        ];

        $scope.skillRanks = [
            { key: "0", val: 0 },
            { key: '1', val: 1 },
            { key: '2', val: 2 },
            { key: '3', val: 3 },
            { key: '4', val: 4 },
            { key: '5', val: 5 }
        ];

        $scope.putInterview = function () {

            $scope.interview.status = interviewsService.statusManager(
                $scope.interview.type,
                $scope.interview.feedbackMessage
            );

            interviewsService.putInterview($scope.interview.id, $scope.interview);
            $mdDialog.cancel();
        };


        $scope.candidateDetails = function () {
            $state.go('candidate', { id: $scope.interview.candidate.id });
            $mdDialog.cancel();
        };

        $scope.getAccess = function () {
            return localStorage.getItem('userRole');
        };
    }
})());