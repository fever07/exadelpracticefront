((() => {
    'use strict';
    angular.module('app')
        .controller('InterviewsDetailsController', InterviewsDetailsController);

    InterviewsDetailsController.$inject = [
        '$scope',
        '$mdToast',
        'interviewsService',
        'id'
    ];

    function InterviewsDetailsController($scope, $mdToast, interviewsService, id) {
        interviewsService.getInterview(id).then(response => {
            $scope.interview = response;
        })
            .catch(error => {
                $mdToast.show($mdToast.simple()
                    .textContent(`${error.statusText}`)
                    .position('bottom right')
                    .toastClass('vladloh'));
            })
            .finally(() => {
                $scope.isLoading = false;
            });

        $scope.candidateDetails = function () {
            $state.go('candidate', {id: $scope.interview.candidate.id});
            $mdDialog.cancel();
        }
    }
})());
