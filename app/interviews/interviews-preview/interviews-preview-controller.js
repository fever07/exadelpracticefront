((() => {
    'use strict';
    angular.module('app')
        .controller('InterviewsPreviewController', InterviewsPreviewController);

    InterviewsPreviewController.$inject = [
        '$scope',
        '$state',
        '$mdDialog',
        'interviewsService',
        'id'
    ];

    function InterviewsPreviewController($scope, $state, $mdDialog, interviewsService, id) {
        interviewsService.getInterview(id).then(response => {
            $scope.interview = response;
        })
            .catch(error => {
                $mdToast.show($mdToast.simple()
                    .textContent(`${error.statusText}`)
                    .position('bottom right')
                    .toastClass('vladloh'));
            })
            .finally(() => {
                $scope.isLoading = false;
            });

        $scope.candidateDetails = function () {
            $state.go('candidate', {id: $scope.interview.candidate.id});
            $mdDialog.cancel();
        };

        $scope.getAccess = function () {
            return localStorage.getItem('userRole');
        };
    }
})());
