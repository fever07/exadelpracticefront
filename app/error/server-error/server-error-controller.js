((() => {

    'use strict';
    angular.module('app')
        .controller('serverErrorController', serverErrorController);

    serverErrorController.$inject = ['$scope', '$stateParams'];

    function serverErrorController($scope, $stateParams) {

        $scope.error = $stateParams.error;
    }

})());