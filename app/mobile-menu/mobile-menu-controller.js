(function () {
    'use strict';

    angular.module('app').controller('MobileMenuController', MobileMenuController);

    MobileMenuController.$inject = [
        '$scope',
        '$timeout',
        '$mdSidenav',
        '$log'
    ];


    function MobileMenuController($scope, $timeout, $mdSidenav, $log) {

        const vm = this;
        vm.toggleRight = buildToggler('right');
        vm.isOpenRight = function () {
            return $mdSidenav('right').isOpen();
        };

        function debounce(func, wait, context) {
            let timer;

            return function debounced() {
                let context = $scope,
                    args = Array.prototype.slice.call(arguments);
                $timeout.cancel(timer);
                timer = $timeout(function () {
                    timer = undefined;
                    func.apply(context, args);
                }, wait || 10);
            };
        }

        function buildDelayedToggler(navID) {
            return debounce(function () {
                $mdSidenav(navID)
                    .toggle()
                    .then(function () {
                        $log.debug("toggle " + navID + " is done");
                    });
            }, 200);
        }

        function buildToggler(navID) {
            return function () {
                $mdSidenav(navID)
                    .toggle()
                    .then(function () {
                        $log.debug("toggle " + navID + " is done");

                    });
            };
        }

        vm.closeMenu = function () {
            $mdSidenav('right').close();
        };
    }
}());