((() => {
    angular.module('app')
        .factory('candidatesApiService', candidatesApiService);

    candidatesApiService.$inject = ['$http', 'appConstants', 'loginService', 'fileWorkerService'];

    function candidatesApiService($http, appConstants, loginService, fileWorkerService) {

        const service = {};

        service.getCandidates = (tab, filter) => {
            filter = filter || {};
            return $http({
                url: `${appConstants.apiUrl}/Candidates/${tab}`,
                method: 'GET',
                params: filter,
                headers: {
                    'Authorization': `Bearer ${loginService.getToken()}`,
                }
            })
                .then(response => response.data);
        };

        service.getCandidateDetails = (id) => {
            return $http({
                url: `${appConstants.apiUrl}/Candidates/${id}`,
                method: 'GET',
                headers: {
                    'Authorization': `Bearer ${loginService.getToken()}`,
                }
            })
                .then(response => {
                    return response.data;
                });

        };

        service.postCandidate = candidate => $http({
            method: 'POST',
            url: `${appConstants.apiUrl}/Candidates`,
            data: candidate,
            withCredentials: true,
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${loginService.getToken()}`,
            }
        });

        service.postForceCandidate = candidate => $http({
            method: 'POST',
            url: `${appConstants.apiUrl}/Candidates/force`,
            data: candidate,
            withCredentials: true,
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${loginService.getToken()}`,
            }
        });

        service.putCandidate = (id, candidate) => $http({
            method: 'PUT',
            url: `${appConstants.apiUrl}/Candidates/${id}`,
            data: candidate,
            withCredentials: true,
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${loginService.getToken()}`,
            }
        });

        service.getCandidateHistory = (id, query) => {

            return $http({
                method: 'GET',
                url: `${appConstants.apiUrl}/history/candidate/${id}`,
                params: query,
                headers: {
                    'Authorization': `Bearer ${loginService.getToken()}`,
                }
            })
                .then(response => response.data)
        };

        service.getCandidateResume = (id) => {

            return fileWorkerService.getFile(`Resume/${id}`)
        };

        service.getReport = (tab, filter) => {

            return fileWorkerService.getFile(`Report/${tab}`, filter)

        };

        return service;

    }
})());
