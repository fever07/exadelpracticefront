((() => {
    'use strict';
    angular.module('app')
        .directive('candidateCard', () => ({
            templateUrl: 'candidates/candidate-card/candidate-card.html',
            scope: {
                candidate: '<',
                filterConfig: '<',
            },
            controller: CandidateCardController,
        }));


    CandidateCardController.$inject = ['$scope', '$state'];

    function CandidateCardController($scope, $state) {

        $scope.candidateDetails = id => {

            const selectedFilter = $scope.filterConfig;
            $state.go('candidate', { id, selectedFilter });

        }
    }
})());
