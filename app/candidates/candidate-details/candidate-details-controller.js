/* eslint-disable angular/controller-as */
((() => {
    'use strict';
    angular.module('app')
        .controller('CandidateDetailsController', CandidateDetailsController);

    CandidateDetailsController.$inject = [
        '$scope', '$state', '$stateParams',
        'candidatesApiService', '$mdToast', '$mdDialog', '$document'];

    function CandidateDetailsController($scope, $state, $stateParams, candidatesApiService, $mdToast, $mdDialog, $document) {

        $scope.isLoading = true;
        $scope.limit = 3;


        candidatesApiService.getCandidateDetails($stateParams.id)
            .then(response => {
                $scope.candidate = response;
            }, error => {
                $state.go('error', { error }, { location: false })
            })
            .finally(() => {
                $scope.isLoading = false;
            });

        $scope.goBack = () => {
            $state.go('candidates', { selectedFilter: $stateParams.selectedFilter });
        };

        $scope.addCandidate = () => {
            $state.go('add-candidate');
        };

        $scope.getResume = (id) => {
            candidatesApiService.getCandidateResume(id)
                .then(() => {
                    $mdToast.show($mdToast.simple()
                        .textContent('Success!')
                        .position('bottom right')
                        .toastClass('vladloh'));
                })
                .catch(error => {
                    $mdToast.show($mdToast.simple()
                        .textContent(`${error.statusText}`)
                        .position('bottom right')
                        .toastClass('vladloh'));
                });
        };

        $scope.addInterview = () => {
            $mdDialog.show({
                controller: 'InterviewsAddController',
                templateUrl: 'interviews/interviews-add/interviews-add.html',
                parent: angular.element($document.body),
                clickOutsideToClose: true,
                locals: {
                    candidate: $scope.candidate
                }
            });
        };

        $scope.getInterview = (id) => {

            $mdDialog.show({
                controller: 'InterviewsDetailsController',
                templateUrl: 'interviews/interviews-details/interviews-details.html',
                parent: angular.element($document.body),
                clickOutsideToClose: true,
                locals: {
                    id
                }
            })
        };

        $scope.getMoreInterviews = () => {

            if ($scope.limit === 3) {
                $scope.limit = $scope.candidate.interviews.length;
            }
        };

        $scope.getHistory = (id) => {
            $mdDialog.show({
                parent: angular.element($document.body),
                templateUrl: 'history/history.html',
                clickOutsideToClose: true,
                controller: 'HistoryController',
                locals: {
                    id,
                    loadCallback: candidatesApiService.getCandidateHistory
                }
            });

        };

        $scope.editCandidate = (id) => {

            $state.go('edit-candidate', { id })

        };

    }
})());
