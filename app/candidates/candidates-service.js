((() => {
    'use strict';

    angular.module('app')
        .factory('candidatesService', candidatesService);

    candidatesService.$inject = ['staticService'];

    function candidatesService(staticService) {

        const service = {};

        const tabs = ['Active', 'Archive'];

        let filters = {
            city: [],
            profession: [],
            experience: [],
            englishLevel: [],
            skills: [],
            candidateStatuses: [],
        };

        service.getTabs = () => tabs;

        service.getFilters = () => {
            return staticService.getConfig(filters)
        };

        return service;

    }
})());