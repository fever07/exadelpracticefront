(() => {
    'use strict';

    angular.module('app').config(['$stateProvider', candidatesConfig]);

    function candidatesConfig($stateProvider) {
        $stateProvider.state({
            name: 'candidates',
            url: '/candidates',
            templateUrl: 'candidates/candidates.html',
            controller: 'CandidatesController',
            params: {
                selectedFilter: null,
            },
            data: {
                authorizedRoles: ['admin', 'hrm']
            }
        }).state({
            name: 'candidate',
            url: '/candidates/:id',
            templateUrl: 'candidates/candidate-details/candidate-details.html',
            controller: 'CandidateDetailsController',
            params: {
                id: null,
                selectedFilter: null,
            },
            data: {
                authorizedRoles: ['admin', 'hrm']
            }
        });

    }

})();
