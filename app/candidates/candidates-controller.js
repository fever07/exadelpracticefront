((() => {

    'use strict';

    angular.module('app')
        .controller('CandidatesController', CandidatesController);

    CandidatesController.$inject = ['$scope', '$stateParams', 'candidatesService', 'candidatesApiService', '$mdToast'];

    function CandidatesController($scope, $stateParams, candidatesService, candidatesApiService, $mdToast) {
        $scope.filters = {};
        candidatesService.getFilters()
            .then(response => $scope.filters = response, reject => {
                $mdToast.show($mdToast.simple()
                    .textContent(`${reject.statusText}`)
                    .position('bottom right')
                    .toastClass('vladloh'));
            });

        $scope.tabs = candidatesService.getTabs();
        $scope.activeTab = $scope.tabs[0];
        $scope.candidates = {};
        $scope.tabs.forEach(tab => $scope.candidates[tab] = []);
        $scope.alreadySelected = $stateParams.selectedFilter || {};
        init();
        $scope.filterConfig = $stateParams.selectedFilter;

        $scope.$on('quickSearch:applyFilter', (event, filterConfig) => {
            init();
            $scope.filterConfig = filterConfig;
            $scope.loadMore();
        });

        $scope.changeTab = tab => {
            init();
            $scope.activeTab = tab;
            $scope.$broadcast('quickSearch:clear');
            $scope.loadMore();

        };

        $scope.loadMore = () => {
            $scope.filterConfig = Object.assign({}, $scope.filterConfig, $scope.params);
            loadWithFilter($scope.activeTab, $scope.filterConfig);
            $scope.params.skip += $scope.params.amount;
        };

        $scope.downloadReport = (filterConfig) => {
            filterConfig.skip = 0;
            $mdToast.show($mdToast.simple()
                .textContent('Processing...')
                .position('bottom right')
                .toastClass('vladloh'));
            candidatesApiService.getReport($scope.activeTab, filterConfig)
                .catch(error => {
                    $mdToast.show($mdToast.simple()
                        .textContent(`${error.statusText}`)
                        .position('bottom right')
                        .toastClass('vladloh'));
                });

        };

        function init() {
            $scope.filterConfig = {};
            $scope.found = ' ';
            $scope.showMore = true;
            $scope.candidates[$scope.activeTab] = null;
            $scope.candidates[$scope.activeTab] = [];
            $scope.isLoading = true;
            $scope.params = {
                skip: 0,
                amount: 10,
            };
            $scope.mode = {
                name: 'candidates',
            }
        }

        function loadWithFilter(tab, filterConfig) {
            $scope.isLoading = true;
            $scope.showMore = false;
            candidatesApiService.getCandidates(tab, filterConfig)
                .then(response => {
                    $scope.candidates[tab] = $scope.candidates[tab].concat(response.candidates);
                    $scope.found = response.amount;
                })
                .catch(error => {
                    $mdToast.show($mdToast.simple()
                        .textContent('An error has occurred')
                        .position('bottom right')
                        .toastClass('vladloh'));
                })
                .finally(() => {
                    $scope.isLoading = false;
                    $scope.showMore = $scope.params.skip <= $scope.found;
                });
        }
    }

})());