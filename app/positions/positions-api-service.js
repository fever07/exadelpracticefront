((() => {

    angular.module('app')
        .factory('positionApiService', positionApiService);

    positionApiService.$inject = ['$http', 'appConstants', 'loginService'];

    function positionApiService($http, appConstants, loginService) {

        const service = {};

        service.getPositions = (tab, filter) => {

            filter = filter || {};
            return $http({
                url: `${appConstants.apiUrl}/Positions/${tab}`,
                method: 'GET',
                params: filter,
                headers: {
                    'Authorization': `Bearer ${loginService.getToken()}`,
                }
            })
                .then(response => response.data);
        };

        service.getPositionDetails = (id) => {
            return $http({
                url: `${appConstants.apiUrl}/Positions/${id}`,
                method: 'GET',
                headers: {
                    'Authorization': `Bearer ${loginService.getToken()}`,
                }
            })
                .then(response => {
                    return response.data;
                });

        };

        service.postPosition = (position) => {

            return $http({
                method: 'POST',
                url: `${appConstants.apiUrl}/Positions`,
                data: position,
                withCredentials: true,
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${loginService.getToken()}`,
                }
            });

        };

        service.putPosition = (id, position) => {

            return $http({
                method: 'PUT',
                url: `${appConstants.apiUrl}/Positions/${id}`,
                data: position,
                withCredentials: true,
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${loginService.getToken()}`,
                }
            });

        };

        service.getPositionHistory = (id, query) => {

            return $http({
                method: 'GET',
                url: `${appConstants.apiUrl}/history/position/${id}`,
                params: query,
                headers: {
                    'Authorization': `Bearer ${loginService.getToken()}`,
                }
            })
                .then(response => response.data)
        };

        return service;
    }

})());
