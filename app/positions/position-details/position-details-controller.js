(() => {

    'use strict';

    angular.module('app')
        .controller('PositionDetailsController', PositionDetailsController);

    PositionDetailsController.$inject = ['$scope', '$state', '$stateParams', 'positionApiService', 'candidatesApiService', '$mdDialog', '$document'];

    function PositionDetailsController($scope, $state, $stateParams, positionApiService, candidatesApiService, $mdDialog, $document) {
        const topFilter = {};
        $scope.isLoading = true;
        $scope.limit = 3;
        $scope.topThreeCandidates = [];
        $scope.topLoading = true;

        $scope.goBack = () => {
            $state.go('positions', { selectedFilter: $stateParams.selectedFilter });
        };

        positionApiService.getPositionDetails($stateParams.id)
            .then(response => {
                    $scope.position = response;
                    $scope.isLoading = false;
                },
                error => {
                    $state.go('error', { error }, { location: false })
                })
            .then(() => {
                topFilter.amount = 3;
                topFilter.skills = $scope.position.skills.map(skill => skill.id);
                topFilter.cities = [$scope.position.city.id];
                topFilter.professions = [$scope.position.profession.id];
                return candidatesApiService.getCandidates('active', topFilter)
            })
            .then(response => $scope.topThreeCandidates = response.candidates)
            .finally(() => {
                $scope.topLoading = false;
            });

        $scope.addPosition = () => {
            $state.go('add-position');
        };

        $scope.getHistory = (id) => {

            $mdDialog.show({
                parent: angular.element($document.body),
                templateUrl: 'history/history.html',
                clickOutsideToClose: true,
                controller: 'HistoryController',
                locals: {
                    id,
                    loadCallback: positionApiService.getPositionHistory
                }
            })
        };

        $scope.editPosition = (id) => {
            $state.go('edit-position', { id })
        };

        $scope.getCandidatesForPosition = () => {

            topFilter.amount = 10;

            $state.go('candidates', { selectedFilter: topFilter })
        }
    }

})();

