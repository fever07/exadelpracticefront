(() => {

    'use strict';

    angular.module('app')
        .controller('PositionsController', PositionsController);

    PositionsController
        .$inject = ['$scope', '$stateParams', 'positionsService', 'positionApiService', '$mdToast'];

    function PositionsController($scope, $stateParams, positionsService, positionApiService, $mdToast) {
        $scope.positions = {};
        $scope.filters = {};
        $scope.alreadySelected = $stateParams.selectedFilter || {};

        $scope.filters = positionsService.getFilters()
            .then(response => $scope.filters = response, reject => {
                $mdToast.show($mdToast.simple()
                    .textContent(`${reject.statusText}`)
                    .position('bottom right')
                    .toastClass('vladloh'));
            });

        $scope.tabs = positionsService.getTabs();

        $scope.activeTab = $scope.tabs[0];

        $scope.tabs.forEach(tab => $scope.positions[tab] = []);
        init();
        $scope.filterConfig = $stateParams.selectedFilter;

        $scope.$on('quickSearch:applyFilter', (event, filterConfig) => {
            init();
            $scope.filterConfig = filterConfig;
            $scope.loadMore();
        });

        $scope.changeTab = (tab) => {
            init();
            $scope.activeTab = tab;
            $scope.$broadcast('quickSearch:clear');
            $scope.loadMore()

        };

        $scope.loadMore = () => {
            $scope.filterConfig = Object.assign({}, $scope.filterConfig, $scope.params);
            loadWithFilter($scope.activeTab, $scope.filterConfig);
            $scope.params.skip += $scope.params.amount;
        };

        function init() {
            $scope.params = {
                skip: 0,
                amount: 10,
            };
            $scope.filterConfig = {};
            $scope.found = ' ';
            $scope.positions[$scope.activeTab] = null;
            $scope.positions[$scope.activeTab] = [];
            $scope.showMore = true;
            $scope.isLoading = true;
            $scope.mode = {
                name: 'positions',
            }
        }

        function loadWithFilter(tab, filterConfig) {
            $scope.showMore = false;
            $scope.isLoading = true;
            positionApiService.getPositions(tab, filterConfig)
                .then(response => {
                    $scope.positions[tab] = $scope.positions[tab].concat(response.positions);
                    $scope.found = response.amount;
                })
                .catch(error => {
                    $mdToast.show($mdToast.simple()
                        .textContent('An error has occurred')
                        .position('bottom right')
                        .toastClass('vladloh'));
                })
                .finally(() => {
                    $scope.isLoading = false;
                    $scope.showMore = $scope.params.skip <= $scope.found;
                });
        }

    }

})();
