(() => {
    'use strict';

    angular.module('app')
        .config(positionConfig);

    positionConfig.$inject = ['$stateProvider'];

    function positionConfig($stateProvider) {

        $stateProvider.state({
            name: 'positions',
            url: '/positions',
            templateUrl: 'positions/positions.html',
            controller: 'PositionsController',
            params: {
                selectedFilter: null,
            },
            data: {
                authorizedRoles: ['admin', 'hrm']
            }
        }).state({
            name: 'position',
            url: '/positions/:id',
            templateUrl: 'positions/position-details/position-details.html',
            controller: 'PositionDetailsController',
            params: {
                id: null,
                selectedFilter: null,
            },
            data: {
                authorizedRoles: ['admin', 'hrm']
            }
        });
    }

})();