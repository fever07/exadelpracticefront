((() => {

    'use strict';

    angular.module('app')
        .factory('positionsService', positionsService);

    positionsService.$inject = ['staticService'];

    function positionsService(staticService) {

        const service = {};

        const tabs = ['Active', 'Archive'];

        const filters = {
            city: [],
            profession: [],
            experience: [],
            positionStatuses: [],
        };

        service.getTabs = () => tabs;

        service.getFilters = () => {
            return staticService.getConfig(filters);
        };

        return service;

    }

})());

