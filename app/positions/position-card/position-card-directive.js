(() => {

    'use strict';

    angular.module('app')
        .directive('positionCard', () => ({
            templateUrl: 'positions/position-card/position-card.html',
            scope: {
                position: '<',
                filterConfig: '<',
            },
            controller: PositionCardController,
        }));

    PositionCardController.$inject = ['$scope', '$state'];

    function PositionCardController($scope, $state) {

        $scope.positionDetails = id => {
            const selectedFilter = $scope.filterConfig;
            $state.go('position', { id, selectedFilter });

        }

    }
})();
