(function () {

    'use strict';

    angular.module('app')
        .factory('headerService', headerService);

    function headerService() {
        const states = [{
            name: 'positions',
            url: '/positions',
            templateUrl: 'positions/positions.html'
        }, {
            name: 'candidates',
            url: '/candidates',
            templateUrl: 'candidates/candidates.html'
        }
        ];

        function getStates() {
            return states;
        }

        function getInitState() {
            return states[1];
        }

        return {
            getStates,
            getInitState,
        };
    }

}());
