(function () {

    'use strict';

    angular.module('app').controller('HeaderController', HeaderController);

    HeaderController.$inject = [
        '$state',
        'loginService',
        'headerService',
        'notificationsHubService',
        '$mdToast',
    ];

    function HeaderController($state, loginService, headerService, notificationsHubService, $mdToast) {

        this.states = headerService.getStates();

        this.state = headerService.getInitState();

        this.changeTab = function (state) {
            this.state = state;
            $state.go(state.name);
        };

        this.getAccess = function () {
            return localStorage.getItem('userRole');
        };

        this.getName = function () {
            return localStorage.getItem('userRole').slice(0, 3);
        };


        this.getEmail = function () {
            return localStorage.getItem('userEmail');
        };


        this.logOut = function () {
            notificationsHubService.stop();
            loginService.logOut();
            $state.go('login');
            $mdToast.hide();
        };
    }

}());