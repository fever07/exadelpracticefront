((() => {

    'use strict';

    angular.module('app')
        .factory('adminService', adminService);

    adminService.$inject = ['$http', 'appConstants', 'loginService'];

    function adminService($http, appConstants, loginService) {

        const service = {};

        service.getHistory = (query) => {

            return $http({
                method: 'GET',
                url: `${appConstants.apiUrl}/history`,
                params: query,
                headers: {
                    'Authorization': `Bearer ${loginService.getToken()}`,
                },
            })
        };

        service.postCity = (city) => {
            return $http({
                method: 'POST',
                data: { name: city },
                url: `${appConstants.apiUrl}/cities`,
                withCredentials: true,
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${localStorage.getItem('token')}`,
                }
            })
        };

        service.postCandidateStatus = (candidateStatus) => {
            return $http({
                method: 'POST',
                data: { name: candidateStatus },
                url: `${appConstants.apiUrl}/candidateStatus`,
                withCredentials: true,
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${localStorage.getItem('token')}`,
                }
            })
        };

        service.postPositionStatus = (positionStatus) => {
            return $http({
                method: 'POST',
                data: { name: positionStatus },
                url: `${appConstants.apiUrl}/positionStatus`,
                withCredentials: true,
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${localStorage.getItem('token')}`,
                }
            })
        };

        service.postProfession = (profession) => {
            console.log(profession);
            return $http({
                method: 'POST',
                data: { name: profession },
                url: `${appConstants.apiUrl}/professions`,
                withCredentials: true,
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${localStorage.getItem('token')}`,
                }
            })
        };

        service.postProject = (project) => {
            return $http({
                method: 'POST',
                data: { name: project },
                url: `${appConstants.apiUrl}/projects`,
                withCredentials: true,
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${localStorage.getItem('token')}`,
                }
            })
        };


        return service;

    }

})());

