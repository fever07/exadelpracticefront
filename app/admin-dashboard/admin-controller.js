((() => {

    'use strict';

    angular.module('app')
        .controller('AdminController', AdminController);

    AdminController.$inject = ['$scope', '$state', 'adminService', '$mdToast'];

    function AdminController($scope, $state, adminService,  $mdToast) {

        $scope.history = [];
        $scope.showMore = true;

        $scope.query = {
            skip: 0,
            amount: 10,
        };
        $scope.city = "";
        $scope.statusCandidate = "";
        $scope.statusPosition = "";
        $scope.proffesion = "";
        $scope.project = "";

        $scope.postCity = () => {
            adminService.postCity($scope.city);
            $scope.city = "";
            $scope.messageAdmin ="You've added a new city!"
        };

        $scope.postCandidateStatus = () => {
            adminService.postCandidateStatus($scope.statusCandidate);
            console.log($scope.statusCandidate);
            console.log("--------------");
            $scope.statusCandidate = "";
            $scope.messageAdmin ="You've added a new candidate status!"
        };

        $scope.postPositionStatus = () => {
            adminService.postPositionStatus($scope.statusPosition);
            $scope.statusPosition = "";
            $scope.messageAdmin="You've added a new position status!"
        };
        $scope.postProfession = () => {
            adminService.postProfession($scope.profession);
            $scope.profession = "";
            $scope.messageAdmin="You've added a new profession!"
        };

        $scope.postProject = () => {
            adminService.postProject($scope.project);
            $scope.project = "";
            $scope.messageAdmin="You've added a new project!"
        };


        $scope.loadMore = () => {

            $scope.showMore = false;
            adminService.getHistory($scope.query)
                .then(response => {
                    if (response.data.length !== 0) {
                        $scope.showMore = true;
                        $scope.query.skip += $scope.query.amount;
                    }
                    $scope.history = $scope.history.concat(response.data);
                    console.log($scope.history);
                })
                .catch(error => {
                    $state.go('error', {error});
                });
        };
        $scope.loadMore();

    }
})());
