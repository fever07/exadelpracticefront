((() => {
    'use strict';

    angular.module('app')
        .value('appConstants', {
            apiUrl: '',
            url: '',

            //Add candidate constants
            FOUND_SIMILAR_CANDIDATE_STATUS: 202,
            DEFAULT_CANDIDATE_STATUS: 'Pool',
            DEFAULT_POSITION_STATUS: 'Active',
            LAST_USAGE_MIN_YEAR: 2000,
            LAST_USAGE_MAX_YEAR: (new Date()).getFullYear(),
            SKILL_MIN_RATING: 0,
            SKILL_MAX_RATING: 5,
        });

})());