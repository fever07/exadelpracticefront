((() => {

    'use strict';

    angular.module('app').config(config);

    config.$inject = [
        '$locationProvider',
        '$stateProvider',
        '$urlRouterProvider',
        '$mdThemingProvider'
    ];

    function config($locationProvider, $stateProvider, $urlRouterProvider, $mdThemingProvider) {
        $stateProvider.state({
            name: 'login',
            url: '/login',
            templateUrl: '../login/login.html',
            data: {
                authorizedRoles: ['admin', 'hrm', 'tech']
            }
        }).state({
            name: 'dashboard',
            url: '/dashboard',
            templateUrl: '../admin-dashboard/admin-dashboard.html',
            controller: 'AdminController',
            data: {
                authorizedRoles: ['admin']
            }
        }).state({
            name: 'notifications',
            url: '/notifications',
            templateUrl: '../notifications/notifications.html',
            controller: 'NotificationsController',
            data: {
                authorizedRoles: ['admin', 'hrm', 'tech']
            }
        }).state({
            name: 'error',
            url: '/error',
            templateUrl: '../error/server-error/server-error.html',
            params: {
                error: null,
            },
            controller: 'serverErrorController',
            data: {
                authorizedRoles: ['admin', 'hrm', 'tech']
            }
        });


        $mdThemingProvider.definePalette('ivBlue', {
            '50': 'e3e8eb',
            '100': 'b9c6cd',
            '200': '8aa1ac',
            '300': '5b7b8b',
            '400': '375e72',
            '500': '144259',
            '600': '123c51',
            '700': '0e3348',
            '800': '0b2b3e',
            '900': '061d2e',
            'A100': '68b8ff',
            'A200': '35a0ff',
            'A400': '0288ff',
            'A700': '007be7',
            'contrastDefaultColor': 'light',
            'contrastDarkColors': [
                '50',
                '100',
                '200',
                'A100',
                'A200'
            ],
            'contrastLightColors': [
                '300',
                '400',
                '500',
                '600',
                '700',
                '800',
                '900',
                'A400',
                'A700'
            ]
        });

        $mdThemingProvider.theme('default')
            .primaryPalette('ivBlue');


        $urlRouterProvider.otherwise('/notifications');
        $locationProvider.html5Mode(true);

    }

})());
