((() => {
    'use strict';
    angular.module('app')
        .filter('iconFilter', () => iconName => {
            const icons = {
                'Interview': 'mdi-voice',
                'Interview with customer': 'mdi-account-multiple',
                'Candidate declined': 'mdi-account-remove',
                'CV provided': 'mdi-clipboard-text',
                'Closed': 'mdi-close-circle-outline',
                'Waiting for interview with customer': 'mdi-account-switch',
                'Cancelled': 'mdi-close-circle-outline',
                'Candidate approved': 'mdi-account-plus',
                'Active': 'mdi-fire',
                'On hold': 'mdi-pause',
                'Attention': 'mdi-alert',
                'Job offer rejected': 'mdi-close-circle-outline',
                'Job offer': 'mdi-file-plus',
                'In progress': 'mdi-trending-up',
                'Job offer accepted': 'mdi-check-all',
                'Hired': 'mdi-check-all',
                'Pool': 'mdi-pause',
                'Rejected': 'mdi-close'
            };
            if (icons[iconName]) {
                return icons[iconName];
            }
            return 'help';
        })

})());
