((() => {
    'use strict';

    angular.module('app')
        .factory('staticService', staticService);

    staticService.$inject = ['$http', '$q', 'appConstants', 'loginService'];

    function staticService($http, $q, appConstants, loginService) {

        const service = {};

        const filterRequest = {
            'city': getCities,
            'profession': getProfessions,
            'experience': getExperience,
            'skills': getSkillsList,
            'candidatestatuses': getCandidateStatuses,
            'positionstatuses': getPositionStatuses,
            'englishlevel': getEnglishLevels,
        };

        function getPositionStatuses() {

            return $http({
                url: `${appConstants.apiUrl}/PositionStatus`,
                method: 'GET',
                headers: {
                    'Authorization': `Bearer ${loginService.getToken()}`,
                }
            })
                .then(response => response.data
                );


        }

        function getEnglishLevels() {
            const deferred = $q.defer();

            const values =
                [
                    {
                        name: '>A1',
                        value: '1'
                    },
                    {
                        name: '>A2',
                        value: '2',
                    },
                    {
                        name: '>B1',
                        value: '3'
                    },
                    {
                        name: '>B2',
                        value: '4'
                    },
                    {
                        name: '>C1',
                        value: '5'
                    },
                    {
                        name: '>C2',
                        value: '6'
                    },
                    {
                        name: 'Native',
                        value: '7'
                    }];

            deferred.resolve(values);

            return deferred.promise;
        }

        function getCandidateStatuses() {

            return $http({
                url: `${appConstants.apiUrl}/Candidatestatus`,
                method: 'GET',
                headers: {
                    'Authorization': `Bearer ${loginService.getToken()}`,
                }
            })
                .then(response => response.data);


        }

        function getCities() {

            return $http({
                url: `${appConstants.apiUrl}/Cities`,
                method: 'GET',
                headers: {
                    'Authorization': `Bearer ${loginService.getToken()}`,
                }
            })
                .then(response => response.data);


        }

        function getSkillsTree() {

            return $http({
                url: `${appConstants.apiUrl}/Skills`,
                method: 'GET',
                headers: {
                    'Authorization': `Bearer ${loginService.getToken()}`,
                }
            })
                .then(response => response.data);


        }

        function getProfessions() {


            return $http({
                url: `${appConstants.apiUrl}/Professions`,
                method: 'GET',
                headers: {
                    'Authorization': `Bearer ${loginService.getToken()}`,
                }
            })
                .then(response => response.data);


        }

        function getSkillsList() {

            return $http({
                url: `${appConstants.apiUrl}/Skills/List`,
                method: 'GET',
                headers: {
                    'Authorization': `Bearer ${loginService.getToken()}`,
                }
            })
                .then(response => response.data);
        }

        function getProjects() {

            return $http({
                url: `${appConstants.apiUrl}/Projects`,
                method: 'GET',
                headers: {
                    'Authorization': `Bearer ${loginService.getToken()}`,
                }
            })
                .then(response => response.data);
        }

        function getExperience() {
            const deferred = $q.defer();

            const exp =
                [
                    {
                        name: '>1 year',
                        value: '1'
                    },
                    {
                        name: '>2 years',
                        value: '2',
                    },
                    {
                        name: '>3 years',
                        value: '3'
                    },
                    {
                        name: '>4 years',
                        value: '4'
                    },
                    {
                        name: '>5 years',
                        value: '5'
                    },
                    {
                        name: '>6 years',
                        value: '6'
                    }];

            deferred.resolve(exp);

            return deferred.promise;
        }

        service.getConfig = config => {

            Object.keys(config).forEach((c) => {
                if (filterRequest[c.toLowerCase()]) {
                    config[c] = filterRequest[c.toLowerCase()]();
                }
            });

            return $q.all(config)
                .then(response => {
                    Object.keys(config).forEach((c) => {
                        config[c] = response[c];
                    });
                    return config;
                });
        };

        service.getPositionStatuses = getPositionStatuses;
        service.getCandidateStatuses = getCandidateStatuses;
        service.getCities = getCities;
        service.getProfessions = getProfessions;
        service.getSkillsList = getSkillsList;
        service.getExperience = getExperience;
        service.getEnglishLevels = getEnglishLevels;
        service.getSkillsTree = getSkillsTree;
        service.getProjects = getProjects;

        return service;

    }

})());