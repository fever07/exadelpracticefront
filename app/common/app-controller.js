((() => {

    'use strict';

    angular.module('app').controller('AppController', AppController);

    AppController.$inject = [
        '$scope',
        'loginService',
        'notificationsHubService',
        '$mdToast'
    ];

    function AppController($scope, loginService, notificationsHubService, $mdToast) {

        $scope.isAuthorized = loginService.isAuthorized;

        notificationsHubService.addHandler('displayMessage', notification => {
            $scope.$broadcast('notifications:newNotification', notification);
            $mdToast
                .show($mdToast.notificationPreset()
                    .position('bottom right'));
            let audio = new Audio('vk.mp3');
            audio.play();
        });

        if (loginService.isAuthorized()) {
            notificationsHubService.init();
        }

        if (!loginService.isAuthorized()) {
            notificationsHubService.stop();
        }
    }

})());