((() => {
    'use strict';
    angular.module('app')
        .factory('fileWorkerService', fileWorkerService);

    fileWorkerService.$inject = ['$http', '$q', 'appConstants', '$httpParamSerializer'];

    function fileWorkerService($http, $q, appConstants, $httpParamSerializer) {

        const service = {};

        service.getFile = (resourceLink, query) => {

            const deferred = $q.defer();
            query = query || {};
            query = $httpParamSerializer(query);
            const ifrm = document.createElement('iframe');

            ifrm.onload = (resolve) => {
                if (resolve.status !== 200) {
                    deferred.reject({ statusText: 'Not found' });
                }
                deferred.resolve();
                ifrm.remove();
            };
            ifrm.onerror = (error) => {
                deferred.reject(error);
                ifrm.remove();
            };
            ifrm.style.display = 'none';
            ifrm.setAttribute('src', `${appConstants.apiUrl}/${resourceLink}/?${query}`);
            document.body.appendChild(ifrm);
            return deferred.promise;
        };

        service.postResume = (attachment) => {

            return $http({
                url: `${appConstants.apiUrl}/resume/`,
                method: 'POST',
                withCredentials: true,
                data: attachment,
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${localStorage.getItem('token')}`,
                }
            })

        };


        return service;
    }
})());
