(() => {
    'use strict';
    angular.module('app')
        .filter('englishLevel', () => {
            return (englishNumber) => {
                const englishSkills = ['Unknown', 'A1', 'A2', 'B1', 'B2', 'C1', 'C2', 'Native'];
                return englishSkills[englishNumber] || 'Unknown'
            }
        })

})();
