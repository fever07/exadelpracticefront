(function () {
    
    'use strict';
    
    Array.prototype.top = function () {
        return this[this.length - 1];
    };
    
    Array.prototype.popOut = function () {
        if (this.length > 1)
            this.pop();
    };
    
    Array.prototype.range = function (begin, end, transform) {
        if (!transform)
            transform = (i) => i;
        const arr = [];
        for (let i = begin; i <= end; i++) {
            arr.push(transform(i));
        }
        return arr;
    };
    
    Array.prototype.indexOfBy = function (obj, equal) {
        let ret = -1;
        this.some((elem, index) => {
            if (equal(obj, elem)) {
                ret = index;
                return true;
            }
        });
        return ret;
    };
    
    Array.prototype.difference = function (array, equal) {
        if (!equal)
            equal = (a, b) => {
                return a === b;
            };
        const res = [];
        this.forEach(elem => {
            const index = array.indexOfBy(elem, equal);
            index === -1 && res.push(elem);
        });
        return res;
    }
    
})();