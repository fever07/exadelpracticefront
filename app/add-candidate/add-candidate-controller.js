(function () {
    
    'use strict';
    
    angular.module('app').controller('AddCandidateController', AddCandidateController);
    
    AddCandidateController.$inject = [
        '$scope',
        '$state',
        '$stateParams',
        '$q',
        '$mdDialog',
        'addCandidateService',
        'staticService',
        'candidatesApiService',
        'fileWorkerService',
        'appConstants'
    ];
    
    function AddCandidateController($scope, $state, $stateParams, $q, $mdDialog, addCandidateService, staticService, candidatesApiService, fileWorkerService, appConstants) {
        
        const state = $state.current.name;
        
        if (state === 'edit-candidate') {
            candidatesApiService.getCandidateDetails($stateParams.id)
                .then(response => {
                    $scope.candidate = addCandidateService.deconvertCandidate(response);
                }, error => {
                    console.log(error);
                });
        } else
            $scope.candidate = addCandidateService.getCandidateFromLocal(state);
        
        const inputWrapper = $('.file_upload');
        const inputFile = inputWrapper.find('input')[0];
        
        this.clickFile = function () {
            inputFile.click();
        };
        
        const resume = {};
        
        $scope.fileName = 'File is not chosen';
        inputFile.onchange = function () {
            
            const file = inputFile.files[0];
            const extension = file.name.split('.').pop();
            const reader = new FileReader();
            $scope.fileName = file.name;
            $scope.$apply();
            reader.readAsDataURL(file);
            reader.onload = function () {
                resume.attachment = this.result;
                resume.extension = extension;
            };
            reader.onerror = function (error) {
                console.log('Error: ', error);
            };
            
        };
        
        this.skillsTreeRequest = staticService.getSkillsTree;
        
        this.englishSkillsList = [1, 2, 3, 4, 5, 6, 7,];
        
        this.ratingsList = addCandidateService.ratingsList;
        
        this.yearsList = addCandidateService.listYears;
        
        staticService.getCities().then(res => {
            this.citiesList = res;
        });
        
        staticService.getProfessions().then(res => {
            this.professionsList = res;
        });
        
        staticService.getCandidateStatuses().then(res => {
            this.statusesList = res;
        });
        
        this.addAdditionalSkill = function () {
            
            if (!$scope.candidate.additionalSkills) {
                $scope.candidate.additionalSkills = [];
                $scope.candidate.counterSelects = 0;
            }
            
            $scope.candidate.additionalSkills.push({
                index: $scope.candidate.counterSelects++,
                chosen: '',
                chosenLevel: '',
                chosenYear: null,
                chosenExp: null,
            });
            
        };
        
        this.removeAdditionalSkill = function (index) {
            $scope.candidate.additionalSkills.splice(index, 1);
        };
        
        this.addExtraEmail = function () {
            
            if (!$scope.candidate.extraEmails) {
                $scope.candidate.extraEmails = [];
                $scope.candidate.counterEmails = 0;
            }
            
            $scope.candidate.extraEmails.push({
                index: $scope.candidate.counterEmails++,
                email: '',
            });
            
        };
        
        this.removeExtraEmail = function (index) {
            $scope.candidate.extraEmails.splice(index, 1);
        };
        
        $scope.submit = false;
        
        this.submitCompletely = function (id) {
            $scope.submit = true;
            localStorage.removeItem(state);
            $state.go('candidate', {id: id});
        };
        
        this.showSimilar = function (candidates) {
            $mdDialog.show({
                controller: 'SimilarCandidatesController',
                templateUrl: 'add-candidate/similar-candidates/similar-candidates.html',
                clickOutsideToClose: true,
                locals: {
                    candidates,
                }
            });
        };
        
        this.addCandidateAnyway = function (candidate) {
            this.sendingCandidate = true;
            candidatesApiService.postForceCandidate(addCandidateService.convertCandidate(candidate))
                .then(response => {
                    this.submitCompletely(response.data.id);
                }, error => {
                    this.sendingCandidate = false;
                    this.serverError = error;
                });
        };
        
        this.addResume = function (id) {
            let deferred = $q.defer();
            
            if (resume.attachment) {
                resume.candidateId = id;
                this.sendingResume = true;
                fileWorkerService.postResume(JSON.stringify(resume))
                    .then(response => {
                        this.sendingResume = false;
                        deferred.resolve(response);
                    }, error => {
                        this.sendingResume = false;
                        deferred.resolve(error);
                    });
            } else
                deferred.resolve();
            
            return deferred.promise;
        };
        
        this.addCandidate = function () {
            this.triedToSubmit = true;
            if ($scope.candidateForm.$valid)
                if (state === 'add-candidate' && !this.sendingCandidate && !this.sendingResume) {
                    this.sendingCandidate = true;
                    candidatesApiService.postCandidate(addCandidateService.convertCandidate($scope.candidate))
                        .then(response => {
                            const id = response.data.id;
                            this.sendingCandidate = false;
                            this.serverError = null;
                            if (response.status === appConstants.FOUND_SIMILAR_CANDIDATE_STATUS) {
                                this.similarCandidatesMessage = response.data.message;
                                $scope.similarCandidates = response.data.candidates;
                            } else {
                                this.addResume(id)
                                    .then(respons => {
                                        this.submitCompletely(id);
                                    }, error => {
                                        this.serverError = error;
                                    });
                            }
                        }, error => {
                            this.sendingCandidate = false;
                            this.serverError = error;
                        });
                } else if (state === 'edit-candidate' && !this.sendingCandidate) {
                    const id = $scope.candidate.id;
                    this.sendingCandidate = true;
                    candidatesApiService.putCandidate(id, addCandidateService.convertCandidate($scope.candidate))
                        .then(response => {
                            this.sendingCandidate = false;
                            this.addResume(id)
                                .then(response => {
                                    this.submitCompletely(id);
                                }, error => {
                                    this.serverError = error;
                                })
                        }, error => {
                            this.sendingCandidate = false;
                            this.serverError = error;
                        });
                }
            
        };
    
        //Saving changes on leaving
        $scope.$on('$destroy', function () {
            if (!$scope.submit)
                addCandidateService.setCandidateToLocal($scope.candidate, state);
        });
        
        window.onbeforeunload = function () {
            if (!$scope.submit)
                addCandidateService.setCandidateToLocal($scope.candidate, state);
        };
        
    }
    
}());

