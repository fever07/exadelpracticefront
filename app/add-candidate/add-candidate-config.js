(function () {
    
    'use strict';
    
    angular.module('app').config(config);
    
    config.$inject = [
        '$stateProvider',
    ];
    
    function config($stateProvider) {
        $stateProvider.state({
            name: 'add-candidate',
            url: '/addcandidate',
            templateUrl: 'add-candidate/add-candidate.html',
            controller: 'AddCandidateController as addcandidate',
            data: {
                authorizedRoles: ['admin', 'hrm']
            }
        }).state({
            name: 'edit-candidate',
            url: '/editcandidate/:id',
            templateUrl: 'add-candidate/add-candidate.html',
            controller: 'AddCandidateController as addcandidate',
            params: {
                id: null,
            },
            data: {
                authorizedRoles: ['admin', 'hrm']
            }
        });
    }
    
})();