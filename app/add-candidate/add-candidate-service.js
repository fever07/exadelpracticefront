(function () {
    
    'use strict';
    
    angular.module('app').factory('addCandidateService', addCandidateService);
    
    addCandidateService.$inject = [
        'staticService',
        'appConstants'
    ];
    
    function addCandidateService(staticService, appConstants) {
        
        const ratingsList = Array.prototype.range(appConstants.SKILL_MIN_RATING, appConstants.SKILL_MAX_RATING);
        
        const listYears = Array.prototype.range(appConstants.LAST_USAGE_MIN_YEAR, appConstants.LAST_USAGE_MAX_YEAR);
        
        const numericFields = [
            'counterSelects',
            'counterEmails',
            'salary',
            'experience',
            'level',
            'emglishLevel',
            'primarySkillLastUsage',
            'chosenYear'
        ];
        
        const dateFields = [
            'lastContact',
            'nextContact',
        ];
        
        function candidateParser(key, value) {
            if (numericFields.indexOf(key) !== -1)
                return parseInt(value);
            if (dateFields.indexOf(key) !== -1)
                return new Date(value);
            return value;
        }
        
        function getCandidateFromLocal(state) {
            if (state === 'add-candidate')
                if (!localStorage.getItem('add-candidate'))
                    localStorage.setItem('add-candidate', JSON.stringify({}));
            return JSON.parse(localStorage.getItem(`${state}`), candidateParser);
        }
        
        function setCandidateToLocal(candidate, state) {
            localStorage.setItem(`${state}`, JSON.stringify(candidate));
        }
        
        let statusesList;
        staticService.getCandidateStatuses()
            .then(response => {
                statusesList = response;
            }, error => {
                console.log(error);
            });
        
        function convertCandidate(candidateToConvert) {
            
            const candidate = JSON.parse(JSON.stringify(candidateToConvert), candidateParser);
            const skills = [];
            skills.push({
                id: candidate.primarySkill,
                isPrimary: true,
                experience: candidate.primarySkillExperience,
                rating: candidate.primarySkillLevel,
                lastUsage: candidate.primarySkillLastUsage,
                wasConfirmed: !!candidate.primarySkillWasConfirmed,
            });
            if (candidate.additionalSkills)
                candidate.additionalSkills.forEach(elem => {
                    skills.push({
                        id: elem.chosen,
                        isPrimary: false,
                        experience: elem.chosenExp,
                        rating: elem.chosenLevel,
                        lastUsage: elem.chosenYear,
                        wasConfirmed: !!elem.wasConfirmed,
                    });
                });
            candidate.skills = skills;
            
            const email = [];
            email.push({
                email: candidate.email,
                isPrimary: true,
            });
            if (candidate.extraEmails)
                candidate.extraEmails.forEach(elem => {
                    email.push({
                        email: elem.email,
                        isPrimary: false,
                    });
                });
            candidate.email = email;
            
            //If no status chosen default status set
            if (!candidate.status)
                candidate.status = {
                    id: statusesList.filter(status => status.name === appConstants.DEFAULT_CANDIDATE_STATUS)[0].id,
                };
            
            delete candidate.extraEmails;
            delete candidate.counterSelects;
            delete candidate.counterEmails;
            delete candidate.primarySkill;
            delete candidate.primarySkillExperience;
            delete candidate.primarySkillLevel;
            delete candidate.primarySkillLastUsage;
            delete candidate.additionalSkills;
            
            return JSON.stringify(candidate);
            
        }
        
        function deconvertCandidate(candidate) {
            
            if (candidate.skills) {
                const primarySkill = candidate.skills.filter(skill => skill.isPrimary)[0];
                candidate.primarySkill = primarySkill.id;
                candidate.primarySkillLevel = primarySkill.rating;
                candidate.primarySkillExperience = primarySkill.experience;
                candidate.primarySkillLastUsage = primarySkill.lastUsage;
                candidate.primarySkillWasConfirmed = !!primarySkill.wasConfirmed;
                
                candidate.counterSelects = candidate.skills.length - 1;
                let index = 0;
                candidate.additionalSkills = [];
                candidate.skills.forEach(skill => {
                    if (!skill.isPrimary) {
                        candidate.additionalSkills.push({
                            index: index++,
                            chosen: skill.id,
                            chosenExp: skill.experience,
                            chosenYear: skill.lastUsage,
                            chosenLevel: skill.rating,
                            wasConfirmed: !!skill.wasConfirmed,
                        });
                    }
                });
            }
            
            if (candidate.email) {
                const emails = candidate.email;
                candidate.counterEmails = emails.length - 1;
                let index = 0;
                candidate.email = emails.filter(email => email.isPrimary)[0].email;
                candidate.extraEmails = [];
                emails.forEach(email => {
                    if (!email.isPrimary) {
                        candidate.extraEmails.push({
                            index: index++,
                            email: email.email,
                        });
                    }
                });
            }
            
            if (candidate.lastContact)
                candidate.lastContact = new Date(candidate.lastContact);
            if (candidate.nextContact)
                candidate.nextContact = new Date(candidate.nextContact);
            
            return candidate;
            
        }
        
        return {
            ratingsList,
            listYears,
            setCandidateToLocal,
            getCandidateFromLocal,
            convertCandidate,
            deconvertCandidate,
        };
        
    }
    
}());
