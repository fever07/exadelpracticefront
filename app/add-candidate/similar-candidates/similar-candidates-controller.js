(function () {
    
    'use strict';
    
    angular.module('app').controller('SimilarCandidatesController', similarCandidatesController);
    
    similarCandidatesController.$inject = [
        '$scope',
        'candidates',
        'candidatesApiService'
    ];
    
    function similarCandidatesController($scope, candidates, candidatesApiService) {
        
        $scope.candidates = candidates.map(candidate => {
            candidate.linkText = 'Show';
            candidate.isOpened = false;
            return candidate;
        });
        
        $scope.showCandidateDetails = function (candidate) {
            const newMode = !candidate.isOpened;
            candidate.isOpened = newMode;
            candidate.linkText = newMode ? 'Hide' : 'Show';
        };
        
        $scope.openCandidateDetails = function (candidate) {
            if (!candidate.details) {
                candidate.sendingRequest = true;
                candidatesApiService.getCandidateDetails(candidate.id)
                    .then(response => {
                        candidate.sendingRequest = false;
                        candidate.details = response;
                        $scope.showCandidateDetails(candidate);
                    }, error => {
                        candidate.sendingRequest = false;
                        console.log(error);
                    });
            } else
                $scope.showCandidateDetails(candidate);
        }
        
    }
    
})();