(function () {
    
    'use strict';
    
    angular.module('app').directive('maxLength', () => ({

        restrict: 'A',
        require: 'ngModel',
        link: function (scope, element, attr, ngModel) {
            
            const max = parseInt(attr.maxLength);
            ngModel.$validators.maxLength = function (value) {
                return !value || value.length <= max;
            };

        }

    }));

    angular.module('app').directive('minLength', () => ({

        restrict: 'A',
        require: 'ngModel',
        link: function (scope, element, attr, ngModel) {

            const min = parseInt(attr.minLength);
            ngModel.$validators.minLength = function (value) {
                return !value || value.length >= min;
            };

        }

    }));
    
})();