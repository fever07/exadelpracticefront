(function () {
    
    'use strict';
    
    angular.module('app').directive('email', () => ({
        
        restrict: 'A',
        require: 'ngModel',
        link: function (scope, element, attr, ngModel) {
            
            const emailRegEx = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            ngModel.$validators.email = function (value) {
                return !value || value.match(emailRegEx);
            };
            
        }
        
    }));
    
})();
