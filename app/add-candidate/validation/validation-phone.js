(function () {
    
    'use strict';
    
    angular.module('app').directive('phone', () => ({
        
        restrict: 'A',
        require: 'ngModel',
        link: function (scope, element, attr, ngModel) {
            
            const phoneRegEx = /^\+[0-9]{3,15}$/;
            ngModel.$validators.phone = function (value) {
                return !value || value.match(phoneRegEx);
            };
            
        }
        
    }));
    
})();
