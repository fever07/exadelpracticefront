(function () {
    
    'use strict';
    
    angular.module('app').directive('language', () => ({
        
        restrict: 'A',
        require: 'ngModel',
        link: function (scope, element, attr, ngModel) {
            
            const lang = attr.language;
            let regExp;
            switch (lang) {
                case 'rus':
                    regExp = /[^а-яА-Я]/;
                    break;
                case 'eng':
                    regExp = /[^a-zA-Z]/;
                    break;
            }
            ngModel.$validators.language = function(value) {
                return !value || !value.match(regExp);
            };
            
        }
        
    }));
    
})();