(function () {
    
    'use strict';
    
    angular.module('app').controller('SkillsTreeController', skillsTreeController);
    
    skillsTreeController.$inject = [
        '$scope',
        '$compile',
        '$templateCache'
    ];
    
    function skillsTreeController($scope, $compile, $templateCache) {
        
        const treeFuncPromise = $scope.skillsTreeRequest();
        
        function stringComparator(a, b) {
            if (a.toUpperCase() > b.toUpperCase())
                return 1;
            if (a.toUpperCase() < b.toUpperCase())
                return -1;
            return 0;
        }
        
        function nameComparator(a, b) {
            return stringComparator(a.name, b.name);
        }
        
        function sortChildren(node) {
            
            if (node.children) {
                
                const listHaveChildren = [], listHaveNoChildren = [];
                
                node.children.forEach(elem => elem.children ? listHaveChildren.push(elem) : listHaveNoChildren.push(elem));
                listHaveChildren.sort(nameComparator);
                listHaveNoChildren.sort(nameComparator);
                node.children = listHaveChildren.concat(listHaveNoChildren);
                
                node.children.forEach(sortChildren);
                
            }
            
        }
        
        function sortTree() {
            sortChildren($scope.skillsTree);
        }
        
        function treeTraversal(node, func) {
            node.children && node.children.forEach(elem => {
                func(elem);
                treeTraversal(elem, func)
            });
        }
        
        function toRootTraversal(node, func) {
            if (node.parentId) {
                const parent = $scope.nameObject[node.parentId];
                func(parent);
                toRootTraversal(parent, func);
            }
        }
    
        function countBackDropParams() {
            const visArea = angDoc.documentElement;
        
            return {
                width: visArea.clientWidth,
                height: visArea.clientHeight,
            };
        }
    
        function countTreeParams(e) {
            const rect = e.target.getBoundingClientRect();
            const x = parseInt(rect.left) - 10, y = parseInt(rect.top) - 10;
        
            return {
                x: x,
                y: y,
            };
        }
    
        function createBackDrop() {
        
            //Creating a tree backdrop
            const templateBackDrop = angular.element($templateCache.get('templateBackDrop'))[0];
            const params = countBackDropParams();
            templateBackDrop.style.width = `${params.width}px`;
            templateBackDrop.style.height = `${params.height}px`;
            const backDropElement = $compile(templateBackDrop.outerHTML)($scope)[0];
            angDoc.body.insertBefore(backDropElement, angDoc.body.firstElementChild);
        
        }
    
        function createScroll() {
        
            angDoc.body.style.top = `${-pageYOffset}px`;
            angDoc.body.style.position = 'fixed';
            angDoc.body.style.overflowY = 'scroll';
        
        }
    
        function createTree(e) {
        
            //Creating a tree dropdown
            let templateDiv;
            if ($scope.multiselect)
                templateDiv = angular.element($templateCache.get('templateMultiTree'))[0];
            else
                templateDiv = angular.element($templateCache.get('templateTree'))[0];
            const params = countTreeParams(e);
            templateDiv.style.left = `${params.x}px`;
            templateDiv.style.top = `${params.y}px`;
            // templateDiv.style.visibility = 'hidden';
            //Check if it dont cross the window
            const treeElement = $compile(templateDiv.outerHTML)($scope)[0];
            angDoc.body.appendChild(treeElement);
        
        }
    
        function removeBackDrop() {
            const backdrop = angDoc.getElementsByClassName('tree-backdrop')[0];
            angDoc.body.removeChild(backdrop);
        }
    
        function removeScroll() {
            angDoc.body.style.position = '';
            const yoffset = -parseInt(angDoc.body.style.top);
            angDoc.body.style.top = '';
            angDoc.body.style.overflowY = '';
            window.scrollTo(0, yoffset);
        }
    
        function removeTree() {
            const treeElement = angDoc.getElementsByClassName('tree-content-container')[0];
            angDoc.body.removeChild(treeElement);
        }
    
        function changeStyleIfNeeded() {
            const treeElement = angDoc.getElementsByClassName('content-overlap')[0];
            if (treeElement) {
                const rect = treeElement.getBoundingClientRect();
                const ledgeBot = rect.bottom - angDoc.documentElement.clientHeight;
                const ledgeTop = rect.top;
                //Supposing only one time moving
                if (ledgeBot > 0)
                    treeElement.style.top = `${-ledgeBot - 10}px`;
                if (ledgeTop < 0)
                    treeElement.style.top = `${-ledgeTop + 10}px`;
                // treeElement.style.visibility = 'visible';
            
            }
        }
        
        function initTree() {
            
            $scope.openTree = function (e) {
                createScroll();
                createBackDrop();
                createTree(e);
                setTimeout(changeStyleIfNeeded, 0);
            };
            
            $scope.closeTree = function () {
                removeBackDrop();
                removeScroll();
                removeTree();
            };
    
            $scope.ac = {
                path: [
                    $scope.skillsTree,
                ],
            };
            
            $scope.goToBranch = function (node, ac) {
                ac.path.push(node);
                setTimeout(changeStyleIfNeeded, 0);
            };
            
            $scope.returnToBranch = function (node, index, ac) {
                ac.path = ac.path.slice(0, index + 1);
                setTimeout(changeStyleIfNeeded, 0);
            };
            
            function addSkillNamesByIds(skills) {
                skills.forEach(elem => {
                    if ($scope.chosenSkillName !== '')
                        $scope.chosenSkillName += ', ';
                    $scope.chosenSkillName += $scope.nameObject[elem].name;
                });
            }
    
            function addSkillNames(names) {
                names.forEach(elem => {
                    if ($scope.chosenSkillName !== '')
                        $scope.chosenSkillName += ', ';
                    $scope.chosenSkillName += elem;
                });
            }
            
            function removeSkillNames(skills) {
                const skillNames = skills.map(skill => $scope.nameObject[skill].name);
                const chosenNames = $scope.chosenSkillName.split(', ');
                skillNames.forEach(elem => chosenNames.splice(chosenNames.indexOf(elem), 1));
                $scope.chosenSkillName = '';
                addSkillNames(chosenNames);
            }
            
            if ($scope.multiselect) {
                
                $scope.chosenSkillName = '';
                
                $scope.clear = function () {
        
                    $scope.chosenSkillName = '';
                    treeTraversal($scope.skillsTree, (node) => {
                        node.chosen.value = false;
                        node.indeterminate = false;
                        node.disabled = false;
                    });
        
                };
    
                $scope.selectNode = function (node) {
                    node.chosen.value = true;
                    toRootTraversal(node, elem => elem.indeterminate = true);
                    treeTraversal(node, elem => {
                        if (elem.chosen.value) {
                            $scope.unselectSkill(elem.id);
                            $scope.unselectNode(elem);
                        }
                        elem.disabled = true;
                    });
                };
    
                $scope.unselectNode = function (node) {
                    node.chosen.value = false;
                    toRootTraversal(node, elem => elem.indeterminate = false);
                    treeTraversal(node, elem => elem.disabled = false);
                };
    
                $scope.selectSkill = function (skill) {
                    $scope.chosenSkill.push(skill);
                };
    
                $scope.unselectSkill = function (skill) {
                    $scope.chosenSkill.splice($scope.chosenSkill.indexOf(skill), 1);
                };
    
                $scope.pressNode = function (node) {
                    if (node.chosen.value) {
                        $scope.unselectSkill(node.id);
                        $scope.unselectNode(node);
                    } else {
                        $scope.selectSkill(node.id);
                        $scope.selectNode(node);
                    }
                };
                
                treeTraversal($scope.skillsTree, node => {
                    node.chosen = { value: false };
                });
                
                if (!$scope.chosenSkill || $scope.chosenSkill.length === 0)
                    $scope.chosenSkill = [];
                else
                    treeTraversal($scope.skillsTree, (node) => {
                        const index = $scope.chosenSkill.findIndex(elem => node.id === elem);
                        if (index !== -1) {
                            $scope.selectNode(node);
                            addSkillNamesByIds([node.id]);
                        }
                    });
                
                $scope.$watchCollection('chosenSkill', function (newVal, oldVal) {
                    if (!newVal || newVal.length === 0) {
                        $scope.chosenSkill = [];
                        $scope.chosenSkillName = '';
                        $scope.clear();
                    } else {
                        if (oldVal) {
                            removeSkillNames(oldVal.difference(newVal));
                            addSkillNamesByIds(newVal.difference(oldVal));
                        }
                    }
                });
                
            } else {
                
                if ($scope.chosenSkill && $scope.chosenSkill !== '') {
                    $scope.chosenSkillName = $scope.nameObject[$scope.chosenSkill].name;
                    const newNode = $scope.nameObject[$scope.chosenSkill];
                    toRootTraversal(newNode, elem => {
                        $scope.ac.path.splice(1, 0, elem);
                    });
                }
                
                $scope.selectSkill = function (node) {
                    $scope.chosenSkill = node.id;
                    $scope.closeTree();
                };
                
                $scope.$watch('chosenSkill', function (newVal, oldVal) {
                    if (!newVal || newVal === '') {
                        $scope.chosenSkill = '';
                        $scope.chosenSkillName = '';
                    } else
                        $scope.chosenSkillName = $scope.nameObject[newVal].name;
                    
                });
                
            }
            
        }
        
        const angDoc = angular.element(document)[0];
        
        treeFuncPromise().then(response => {
            
            $scope.skillsTree = response;
            $scope.skillsTree.id = '0';
            
            $scope.nameObject = {};
            
            treeTraversal($scope.skillsTree, node => {
                $scope.nameObject[node.id] = node;
            });
            
            sortTree($scope.skillsTree);
            
            initTree();
            
        }, error => {
            console.log(error);
        });
        
    }
    
})();