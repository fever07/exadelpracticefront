(function () {
    
    'use strict';
    
    angular.module('app').directive('skillsTree', () => ({
        
        templateUrl: 'add-candidate/skills-tree/skills-tree.html',
        scope: {
            skillsTreeRequest: '&',
            chosenSkill: '=',
            multiselect: '<',
            control: '=',
            required: '<',
            mdNoFloat: '<'
        },
        controller: 'SkillsTreeController'
    }));
    
})();
