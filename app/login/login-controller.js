(function () {

    'use strict';


    angular.module('app')
        .controller('LoginController', LoginController);

    LoginController.$inject = [
        '$scope',
        '$state',
        '$mdDialog',
        'loginService',
        'notificationsHubService'
    ];

    function LoginController($scope, $state, $mdDialog, loginService, notificationsHubService) {

        if (loginService.isAuthorized()) {
            $state.go('notifications');
        }

        $scope.submit = (username, password) => {
            loginService.submitLogin(username, password)
                .then(function (response) {
                    loginService.putAccessToken(response.data.access_token);
                    loginService.putUserEmail(username);
                    $state.go('notifications');
                    notificationsHubService.init();
                }, function (response) {
                    $mdDialog.show(
                        $mdDialog.alert()
                            .parent(angular.element(document.querySelector('#popupContainer')))
                            .clickOutsideToClose(true)
                            .title("Sorry!")
                            .textContent(response.data.error_description)
                            .ariaLabel('Alert Dialog Demo')
                            .ok('Ok')
                    );
                });
        }
    }

}());