(function () {

    'use strict';

    angular.module('app')
        .factory('loginService', loginService);

    loginService.$inject = ['$http', 'appConstants', '$state'];

    function loginService($http, appConstants, $state) {
        const service = {};

        service.submitLogin = function (username, password) {
            const request = {
                method: 'POST',
                url: `${appConstants.url}/token`,
                data: `username=${username}&password=${password}&grant_type=password`,
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            };
            return $http(request);
        };


        service.getAccessLevel = function () {
            const request = {
                method: 'GET',
                url: `${appConstants.apiUrl}/users/userinfo/`,
                headers: {
                    'Authorization': `Bearer ${localStorage.getItem('token')}`
                },
            };
            return $http(request);
        };


        service.getHrms = function () {
            const request = {
                method: 'GET',
                url: `${appConstants.apiUrl}/users/hrms`,
                headers: {
                    'Authorization': `Bearer ${localStorage.getItem('token')}`
                },
            };
            return $http(request).then(response => {
                return response.data;
            });
        };

        service.getTechs = function () {
            const request = {
                method: 'GET',
                url: `${appConstants.apiUrl}/users/techs`,
                headers: {
                    'Authorization': `Bearer ${localStorage.getItem('token')}`
                },
            };
            return $http(request).then(response => {
                return response.data;
            });
        };

        service.stateAccess = function (trans) {
            if (service.isAuthorized()) {
                {
                    service.getAccessLevel()
                        .then(function (response) {
                            service.putUserRole(response.data.accessLevel.toLowerCase());
                            let curLevel = service.getUserRole();
                            let flag = true;
                            let curRoles = trans.to().data.authorizedRoles;
                            curRoles.forEach(function (item, curRoles) {
                                if (item === curLevel) {
                                    flag = false;
                                }
                            });
                            if (flag) {
                                $state.go('notifications');
                            }
                        }, function (reject) {
                            if (reject.status === 401) {
                                loginService.logOut();
                                $state.go('login');
                            }
                        });
                }
            }
            else {
                $state.go('login');
            }
        };


        service.putAccessToken = function (token) {
            localStorage.setItem('token', token);
        };

        service.putUserEmail = function (email) {
            localStorage.setItem('userEmail', email);
        };

        service.isAuthorized = function () {
            return localStorage.getItem('token');
        };

        service.getToken = function () {
            return localStorage.getItem('token');
        };

        service.getUserRole = function () {
            return localStorage.getItem('userRole');
        };

        service.putUserRole = function (role) {
            localStorage.setItem('userRole', role);
        };

        service.logOut = function () {
            localStorage.removeItem('token');
            localStorage.removeItem('userRole');
        };

        return service;

    }

}());
