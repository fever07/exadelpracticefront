(() => {
    'use strict';

    angular.module('app', ['ui.router', 'ngMaterial', 'infinite-scroll', 'ngAnimate', 'ngMessages'])
        .run(appRun);
    appRun.$inject = ['$transitions', 'loginService', 'appConstants'];
    function appRun($transitions, loginService, appConstants) {
        const el = document.getElementsByTagName('html');
        appConstants.apiUrl = el[0].getAttribute('data-apiUrl');
        appConstants.url = el[0].getAttribute('data-url');
        $transitions.onStart({}, function (trans) {
            loginService.stateAccess(trans);
        });
    }
})();
(function () {
    
    'use strict';
    
    angular.module('app').config(config);
    
    config.$inject = [
        '$stateProvider',
    ];
    
    function config($stateProvider) {
        $stateProvider.state({
            name: 'add-candidate',
            url: '/addcandidate',
            templateUrl: 'add-candidate/add-candidate.html',
            controller: 'AddCandidateController as addcandidate',
            data: {
                authorizedRoles: ['admin', 'hrm']
            }
        }).state({
            name: 'edit-candidate',
            url: '/editcandidate/:id',
            templateUrl: 'add-candidate/add-candidate.html',
            controller: 'AddCandidateController as addcandidate',
            params: {
                id: null,
            },
            data: {
                authorizedRoles: ['admin', 'hrm']
            }
        });
    }
    
})();
(function () {
    
    'use strict';
    
    angular.module('app').controller('AddCandidateController', AddCandidateController);
    
    AddCandidateController.$inject = [
        '$scope',
        '$state',
        '$stateParams',
        '$q',
        '$mdDialog',
        'addCandidateService',
        'staticService',
        'candidatesApiService',
        'fileWorkerService',
        'appConstants'
    ];
    
    function AddCandidateController($scope, $state, $stateParams, $q, $mdDialog, addCandidateService, staticService, candidatesApiService, fileWorkerService, appConstants) {
        
        const state = $state.current.name;
        
        if (state === 'edit-candidate') {
            candidatesApiService.getCandidateDetails($stateParams.id)
                .then(response => {
                    $scope.candidate = addCandidateService.deconvertCandidate(response);
                }, error => {
                    
                });
        } else
            $scope.candidate = addCandidateService.getCandidateFromLocal(state);
        
        const inputWrapper = $('.file_upload');
        const inputFile = inputWrapper.find('input')[0];
        
        this.clickFile = function () {
            inputFile.click();
        };
        
        const resume = {};
        
        $scope.fileName = 'File is not chosen';
        inputFile.onchange = function () {
            
            const file = inputFile.files[0];
            const extension = file.name.split('.').pop();
            const reader = new FileReader();
            $scope.fileName = file.name;
            $scope.$apply();
            reader.readAsDataURL(file);
            reader.onload = function () {
                resume.attachment = this.result;
                resume.extension = extension;
            };
            reader.onerror = function (error) {
                
            };
            
        };
        
        this.skillsTreeRequest = staticService.getSkillsTree;
        
        this.englishSkillsList = [1, 2, 3, 4, 5, 6, 7,];
        
        this.ratingsList = addCandidateService.ratingsList;
        
        this.yearsList = addCandidateService.listYears;
        
        staticService.getCities().then(res => {
            this.citiesList = res;
        });
        
        staticService.getProfessions().then(res => {
            this.professionsList = res;
        });
        
        staticService.getCandidateStatuses().then(res => {
            this.statusesList = res;
        });
        
        this.addAdditionalSkill = function () {
            
            if (!$scope.candidate.additionalSkills) {
                $scope.candidate.additionalSkills = [];
                $scope.candidate.counterSelects = 0;
            }
            
            $scope.candidate.additionalSkills.push({
                index: $scope.candidate.counterSelects++,
                chosen: '',
                chosenLevel: '',
                chosenYear: null,
                chosenExp: null,
            });
            
        };
        
        this.removeAdditionalSkill = function (index) {
            $scope.candidate.additionalSkills.splice(index, 1);
        };
        
        this.addExtraEmail = function () {
            
            if (!$scope.candidate.extraEmails) {
                $scope.candidate.extraEmails = [];
                $scope.candidate.counterEmails = 0;
            }
            
            $scope.candidate.extraEmails.push({
                index: $scope.candidate.counterEmails++,
                email: '',
            });
            
        };
        
        this.removeExtraEmail = function (index) {
            $scope.candidate.extraEmails.splice(index, 1);
        };
        
        $scope.submit = false;
        
        this.submitCompletely = function (id) {
            $scope.submit = true;
            localStorage.removeItem(state);
            $state.go('candidate', {id: id});
        };
        
        this.showSimilar = function (candidates) {
            $mdDialog.show({
                controller: 'SimilarCandidatesController',
                templateUrl: 'add-candidate/similar-candidates/similar-candidates.html',
                clickOutsideToClose: true,
                locals: {
                    candidates,
                }
            });
        };
        
        this.addCandidateAnyway = function (candidate) {
            this.sendingCandidate = true;
            candidatesApiService.postForceCandidate(addCandidateService.convertCandidate(candidate))
                .then(response => {
                    this.submitCompletely(response.data.id);
                }, error => {
                    this.sendingCandidate = false;
                    this.serverError = error;
                });
        };
        
        this.addResume = function (id) {
            let deferred = $q.defer();
            
            if (resume.attachment) {
                resume.candidateId = id;
                this.sendingResume = true;
                fileWorkerService.postResume(JSON.stringify(resume))
                    .then(response => {
                        this.sendingResume = false;
                        deferred.resolve(response);
                    }, error => {
                        this.sendingResume = false;
                        deferred.resolve(error);
                    });
            } else
                deferred.resolve();
            
            return deferred.promise;
        };
        
        this.addCandidate = function () {
            this.triedToSubmit = true;
            if ($scope.candidateForm.$valid)
                if (state === 'add-candidate' && !this.sendingCandidate && !this.sendingResume) {
                    this.sendingCandidate = true;
                    candidatesApiService.postCandidate(addCandidateService.convertCandidate($scope.candidate))
                        .then(response => {
                            const id = response.data.id;
                            this.sendingCandidate = false;
                            this.serverError = null;
                            if (response.status === appConstants.FOUND_SIMILAR_CANDIDATE_STATUS) {
                                this.similarCandidatesMessage = response.data.message;
                                $scope.similarCandidates = response.data.candidates;
                            } else {
                                this.addResume(id)
                                    .then(respons => {
                                        this.submitCompletely(id);
                                    }, error => {
                                        this.serverError = error;
                                    });
                            }
                        }, error => {
                            this.sendingCandidate = false;
                            this.serverError = error;
                        });
                } else if (state === 'edit-candidate' && !this.sendingCandidate) {
                    const id = $scope.candidate.id;
                    this.sendingCandidate = true;
                    candidatesApiService.putCandidate(id, addCandidateService.convertCandidate($scope.candidate))
                        .then(response => {
                            this.sendingCandidate = false;
                            this.addResume(id)
                                .then(response => {
                                    this.submitCompletely(id);
                                }, error => {
                                    this.serverError = error;
                                })
                        }, error => {
                            this.sendingCandidate = false;
                            this.serverError = error;
                        });
                }
            
        };
    
        //Saving changes on leaving
        $scope.$on('$destroy', function () {
            if (!$scope.submit)
                addCandidateService.setCandidateToLocal($scope.candidate, state);
        });
        
        window.onbeforeunload = function () {
            if (!$scope.submit)
                addCandidateService.setCandidateToLocal($scope.candidate, state);
        };
        
    }
    
}());


(function () {
    
    'use strict';
    
    angular.module('app').factory('addCandidateService', addCandidateService);
    
    addCandidateService.$inject = [
        'staticService',
        'appConstants'
    ];
    
    function addCandidateService(staticService, appConstants) {
        
        const ratingsList = Array.prototype.range(appConstants.SKILL_MIN_RATING, appConstants.SKILL_MAX_RATING);
        
        const listYears = Array.prototype.range(appConstants.LAST_USAGE_MIN_YEAR, appConstants.LAST_USAGE_MAX_YEAR);
        
        const numericFields = [
            'counterSelects',
            'counterEmails',
            'salary',
            'experience',
            'level',
            'emglishLevel',
            'primarySkillLastUsage',
            'chosenYear'
        ];
        
        const dateFields = [
            'lastContact',
            'nextContact',
        ];
        
        function candidateParser(key, value) {
            if (numericFields.indexOf(key) !== -1)
                return parseInt(value);
            if (dateFields.indexOf(key) !== -1)
                return new Date(value);
            return value;
        }
        
        function getCandidateFromLocal(state) {
            if (state === 'add-candidate')
                if (!localStorage.getItem('add-candidate'))
                    localStorage.setItem('add-candidate', JSON.stringify({}));
            return JSON.parse(localStorage.getItem(`${state}`), candidateParser);
        }
        
        function setCandidateToLocal(candidate, state) {
            localStorage.setItem(`${state}`, JSON.stringify(candidate));
        }
        
        let statusesList;
        staticService.getCandidateStatuses()
            .then(response => {
                statusesList = response;
            }, error => {
                
            });
        
        function convertCandidate(candidateToConvert) {
            
            const candidate = JSON.parse(JSON.stringify(candidateToConvert), candidateParser);
            const skills = [];
            skills.push({
                id: candidate.primarySkill,
                isPrimary: true,
                experience: candidate.primarySkillExperience,
                rating: candidate.primarySkillLevel,
                lastUsage: candidate.primarySkillLastUsage,
                wasConfirmed: !!candidate.primarySkillWasConfirmed,
            });
            if (candidate.additionalSkills)
                candidate.additionalSkills.forEach(elem => {
                    skills.push({
                        id: elem.chosen,
                        isPrimary: false,
                        experience: elem.chosenExp,
                        rating: elem.chosenLevel,
                        lastUsage: elem.chosenYear,
                        wasConfirmed: !!elem.wasConfirmed,
                    });
                });
            candidate.skills = skills;
            
            const email = [];
            email.push({
                email: candidate.email,
                isPrimary: true,
            });
            if (candidate.extraEmails)
                candidate.extraEmails.forEach(elem => {
                    email.push({
                        email: elem.email,
                        isPrimary: false,
                    });
                });
            candidate.email = email;
            
            //If no status chosen default status set
            if (!candidate.status)
                candidate.status = {
                    id: statusesList.filter(status => status.name === appConstants.DEFAULT_CANDIDATE_STATUS)[0].id,
                };
            
            delete candidate.extraEmails;
            delete candidate.counterSelects;
            delete candidate.counterEmails;
            delete candidate.primarySkill;
            delete candidate.primarySkillExperience;
            delete candidate.primarySkillLevel;
            delete candidate.primarySkillLastUsage;
            delete candidate.additionalSkills;
            
            return JSON.stringify(candidate);
            
        }
        
        function deconvertCandidate(candidate) {
            
            if (candidate.skills) {
                const primarySkill = candidate.skills.filter(skill => skill.isPrimary)[0];
                candidate.primarySkill = primarySkill.id;
                candidate.primarySkillLevel = primarySkill.rating;
                candidate.primarySkillExperience = primarySkill.experience;
                candidate.primarySkillLastUsage = primarySkill.lastUsage;
                candidate.primarySkillWasConfirmed = !!primarySkill.wasConfirmed;
                
                candidate.counterSelects = candidate.skills.length - 1;
                let index = 0;
                candidate.additionalSkills = [];
                candidate.skills.forEach(skill => {
                    if (!skill.isPrimary) {
                        candidate.additionalSkills.push({
                            index: index++,
                            chosen: skill.id,
                            chosenExp: skill.experience,
                            chosenYear: skill.lastUsage,
                            chosenLevel: skill.rating,
                            wasConfirmed: !!skill.wasConfirmed,
                        });
                    }
                });
            }
            
            if (candidate.email) {
                const emails = candidate.email;
                candidate.counterEmails = emails.length - 1;
                let index = 0;
                candidate.email = emails.filter(email => email.isPrimary)[0].email;
                candidate.extraEmails = [];
                emails.forEach(email => {
                    if (!email.isPrimary) {
                        candidate.extraEmails.push({
                            index: index++,
                            email: email.email,
                        });
                    }
                });
            }
            
            if (candidate.lastContact)
                candidate.lastContact = new Date(candidate.lastContact);
            if (candidate.nextContact)
                candidate.nextContact = new Date(candidate.nextContact);
            
            return candidate;
            
        }
        
        return {
            ratingsList,
            listYears,
            setCandidateToLocal,
            getCandidateFromLocal,
            convertCandidate,
            deconvertCandidate,
        };
        
    }
    
}());

((() => {

    'use strict';

    angular.module('app')
        .controller('AdminController', AdminController);

    AdminController.$inject = ['$scope', '$state', 'adminService', '$mdToast'];

    function AdminController($scope, $state, adminService,  $mdToast) {

        $scope.history = [];
        $scope.showMore = true;

        $scope.query = {
            skip: 0,
            amount: 10,
        };
        $scope.city = "";
        $scope.statusCandidate = "";
        $scope.statusPosition = "";
        $scope.proffesion = "";
        $scope.project = "";

        $scope.postCity = () => {
            adminService.postCity($scope.city);
            $scope.city = "";
            $scope.messageAdmin ="You've added a new city!"
        };

        $scope.postCandidateStatus = () => {
            adminService.postCandidateStatus($scope.statusCandidate);
            
            
            $scope.statusCandidate = "";
            $scope.messageAdmin ="You've added a new candidate status!"
        };

        $scope.postPositionStatus = () => {
            adminService.postPositionStatus($scope.statusPosition);
            $scope.statusPosition = "";
            $scope.messageAdmin="You've added a new position status!"
        };
        $scope.postProfession = () => {
            adminService.postProfession($scope.profession);
            $scope.profession = "";
            $scope.messageAdmin="You've added a new profession!"
        };

        $scope.postProject = () => {
            adminService.postProject($scope.project);
            $scope.project = "";
            $scope.messageAdmin="You've added a new project!"
        };


        $scope.loadMore = () => {

            $scope.showMore = false;
            adminService.getHistory($scope.query)
                .then(response => {
                    if (response.data.length !== 0) {
                        $scope.showMore = true;
                        $scope.query.skip += $scope.query.amount;
                    }
                    $scope.history = $scope.history.concat(response.data);
                    
                })
                .catch(error => {
                    $state.go('error', {error});
                });
        };
        $scope.loadMore();

    }
})());

((() => {

    'use strict';

    angular.module('app')
        .factory('adminService', adminService);

    adminService.$inject = ['$http', 'appConstants', 'loginService'];

    function adminService($http, appConstants, loginService) {

        const service = {};

        service.getHistory = (query) => {

            return $http({
                method: 'GET',
                url: `${appConstants.apiUrl}/history`,
                params: query,
                headers: {
                    'Authorization': `Bearer ${loginService.getToken()}`,
                },
            })
        };

        service.postCity = (city) => {
            return $http({
                method: 'POST',
                data: { name: city },
                url: `${appConstants.apiUrl}/cities`,
                withCredentials: true,
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${localStorage.getItem('token')}`,
                }
            })
        };

        service.postCandidateStatus = (candidateStatus) => {
            return $http({
                method: 'POST',
                data: { name: candidateStatus },
                url: `${appConstants.apiUrl}/candidateStatus`,
                withCredentials: true,
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${localStorage.getItem('token')}`,
                }
            })
        };

        service.postPositionStatus = (positionStatus) => {
            return $http({
                method: 'POST',
                data: { name: positionStatus },
                url: `${appConstants.apiUrl}/positionStatus`,
                withCredentials: true,
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${localStorage.getItem('token')}`,
                }
            })
        };

        service.postProfession = (profession) => {
            
            return $http({
                method: 'POST',
                data: { name: profession },
                url: `${appConstants.apiUrl}/professions`,
                withCredentials: true,
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${localStorage.getItem('token')}`,
                }
            })
        };

        service.postProject = (project) => {
            return $http({
                method: 'POST',
                data: { name: project },
                url: `${appConstants.apiUrl}/projects`,
                withCredentials: true,
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${localStorage.getItem('token')}`,
                }
            })
        };


        return service;

    }

})());


(function () {
    
    'use strict';
    
    angular.module('app').config(config);
    
    config.$inject = [
        '$stateProvider',
    ];
    
    function config($stateProvider) {
        
        $stateProvider.state({
            name: 'add-position',
            url: '/add-position',
            templateUrl: 'add-position/add-position.html',
            controller: 'AddPositionController as addposition',
            data: {
                authorizedRoles: ['admin', 'hrm']
            }
        }).state({
            name: 'edit-position',
            url: '/editposition/:id',
            templateUrl: 'add-position/add-position.html',
            controller: 'AddPositionController as addposition',
            params: {
                id: null,
            },
            data: {
                authorizedRoles: ['admin', 'hrm']
            }
        });
    }
    
})();

(function () {
    
    'use strict';
    
    angular.module('app').controller('AddPositionController', AddPositionController);
    
    AddPositionController.$inject = [
        '$scope',
        '$state',
        '$stateParams',
        'addPositionService',
        'positionApiService',
        'staticService'
    ];
    
    function AddPositionController($scope, $state, $stateParams, addPositionService, positionApiService, staticService) {
        
        const state = $state.current.name;
        
        if (state === 'edit-position') {
            positionApiService.getPositionDetails($stateParams.id)
                .then(response => {
                    this.position = addPositionService.deconvertPosition(response);
                }, error => {
                    
                });
        } else
            this.position = addPositionService.getPositionFromLocal(state);
        
        this.skillsTreeRequest = staticService.getSkillsTree;
        
        staticService.getCities().then(res => {
            this.citiesList = res;
        });
        
        staticService.getProfessions().then(res => {
            this.professionsList = res;
        });
        
        staticService.getPositionStatuses().then(res => {
            this.statusesList = res;
        });
        
        staticService.getProjects().then(res => {
            this.projectsList = res;
        });
        
        this.addAdditionalSkill = function () {
            
            if (!this.position.additionalSkills) {
                this.position.additionalSkills = [];
                this.position.counterSelects = 0;
            }
            
            this.position.additionalSkills.push({
                index: this.position.counterSelects++,
                chosen: '',
                chosenLevel: '',
                chosenYear: null,
                chosenExp: null,
            });
            
        };
        
        this.removeAdditionalSkill = function () {
            
            this.position.additionalSkills.splice(this.position.additionalSkills.length - 1, 1);
            
        };
        
        this.submit = false;
        
        this.submitCompletely = function (id) {
            this.submit = true;
            localStorage.removeItem(state);
            $state.go('position', {id: id});
        };
        
        this.addPosition = function () {
            this.triedToSubmit = true;
            if ($scope.positionForm.$valid)
                if (state === 'add-position') {
                    this.sendingPosition = true;
                    positionApiService.postPosition(addPositionService.convertPosition(this.position))
                        .then(response => {
                            this.sendingPosition = false;
                            this.submitCompletely(response.data.id);
                        }, error => {
                            this.sendingPosition = false;
                            this.serverError = error;
                        });
                } else {
                    const id = this.position.id;
                    this.sendingPosition = true;
                    positionApiService.putPosition(id, addPositionService.convertPosition(this.position))
                        .then(response => {
                            this.sendingPosition = false;
                            this.submitCompletely(id);
                        }, error => {
                            this.sendingPosition = false;
                            this.serverError = error;
                        });
                }
        };
        
        const _this = this;
        
        $scope.$on('$destroy', function () {
            if (!_this.submit)
                addPositionService.setPositionToLocal(_this.position);
        });
        
        window.onbeforeunload = function () {
            if (!_this.submit)
                addPositionService.setPositionToLocal(_this.position);
        };
        
    }
    
}());

(function () {
    
    'use strict';
    
    angular.module('app').factory('addPositionService', addPositionService);
    
    addPositionService.$inject = [
        'appConstants',
        'staticService'
    ];
    
    function addPositionService(appConstants, staticService) {
        
        const dateFields = [
            'requestDate',
            'projectDate',
        ];
        
        const numericFields = [
            'experience',
            'level',
        ];
        
        function positionParser(key, value) {
            if (dateFields.indexOf(key) !== -1)
                return new Date(value);
            if (numericFields.indexOf(key) !== -1)
                return parseInt(value);
            return value;
        }
        
        function setPositionToLocal(newPosition) {
            localStorage.setItem('add-position', JSON.stringify(newPosition));
        }
        
        function getPositionFromLocal() {
            const position = localStorage.getItem('add-position');
            return position ? JSON.parse(position, positionParser) : {};
        }
        
        let statusesList;
        staticService.getPositionStatuses()
            .then(response => {
                statusesList = response;
            });
        
        function convertPosition(positionToConvert) {
            
            const position = JSON.parse(JSON.stringify(positionToConvert), positionParser);
            position.skills = position.skills.map(skill => ({
                id: skill,
                isPrimary: false,
            }));
            position.skills.unshift({
                id: position.primarySkill,
                isPrimary: true,
            });
            
            //If no status chosen default status set
            if (!position.status)
                position.status = {
                    id: statusesList.filter(status => status.name === appConstants.DEFAULT_POSITION_STATUS)[0].id,
                };
            
            delete position.primarySkill;
            delete position.additionalSkills;
            delete position.counterSelects;
            
            return position;
            
        }
        
        function deconvertPosition(position) {
            
            position.primarySkill = position.skills.find(skill => skill.isPrimary).id;
            position.counterSelects = position.skills.length - 1;
            position.additionalSkills = [];
            
            position.skills = position.skills
                .filter(skill => !skill.isPrimary)
                .map(skill => skill.id);
            
            return position;
            
        }
        
        return {
            
            getPositionFromLocal,
            setPositionToLocal,
            convertPosition,
            deconvertPosition,
            
        };
        
    }
    
}());

((() => {
    angular.module('app')
        .factory('candidatesApiService', candidatesApiService);

    candidatesApiService.$inject = ['$http', 'appConstants', 'loginService', 'fileWorkerService'];

    function candidatesApiService($http, appConstants, loginService, fileWorkerService) {

        const service = {};

        service.getCandidates = (tab, filter) => {
            filter = filter || {};
            return $http({
                url: `${appConstants.apiUrl}/Candidates/${tab}`,
                method: 'GET',
                params: filter,
                headers: {
                    'Authorization': `Bearer ${loginService.getToken()}`,
                }
            })
                .then(response => response.data);
        };

        service.getCandidateDetails = (id) => {
            return $http({
                url: `${appConstants.apiUrl}/Candidates/${id}`,
                method: 'GET',
                headers: {
                    'Authorization': `Bearer ${loginService.getToken()}`,
                }
            })
                .then(response => {
                    return response.data;
                });

        };

        service.postCandidate = candidate => $http({
            method: 'POST',
            url: `${appConstants.apiUrl}/Candidates`,
            data: candidate,
            withCredentials: true,
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${loginService.getToken()}`,
            }
        });

        service.postForceCandidate = candidate => $http({
            method: 'POST',
            url: `${appConstants.apiUrl}/Candidates/force`,
            data: candidate,
            withCredentials: true,
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${loginService.getToken()}`,
            }
        });

        service.putCandidate = (id, candidate) => $http({
            method: 'PUT',
            url: `${appConstants.apiUrl}/Candidates/${id}`,
            data: candidate,
            withCredentials: true,
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${loginService.getToken()}`,
            }
        });

        service.getCandidateHistory = (id, query) => {

            return $http({
                method: 'GET',
                url: `${appConstants.apiUrl}/history/candidate/${id}`,
                params: query,
                headers: {
                    'Authorization': `Bearer ${loginService.getToken()}`,
                }
            })
                .then(response => response.data)
        };

        service.getCandidateResume = (id) => {

            return fileWorkerService.getFile(`Resume/${id}`)
        };

        service.getReport = (tab, filter) => {

            return fileWorkerService.getFile(`Report/${tab}`, filter)

        };

        return service;

    }
})());

(() => {
    'use strict';

    angular.module('app').config(['$stateProvider', candidatesConfig]);

    function candidatesConfig($stateProvider) {
        $stateProvider.state({
            name: 'candidates',
            url: '/candidates',
            templateUrl: 'candidates/candidates.html',
            controller: 'CandidatesController',
            params: {
                selectedFilter: null,
            },
            data: {
                authorizedRoles: ['admin', 'hrm']
            }
        }).state({
            name: 'candidate',
            url: '/candidates/:id',
            templateUrl: 'candidates/candidate-details/candidate-details.html',
            controller: 'CandidateDetailsController',
            params: {
                id: null,
                selectedFilter: null,
            },
            data: {
                authorizedRoles: ['admin', 'hrm']
            }
        });

    }

})();

((() => {

    'use strict';

    angular.module('app')
        .controller('CandidatesController', CandidatesController);

    CandidatesController.$inject = ['$scope', '$stateParams', 'candidatesService', 'candidatesApiService', '$mdToast'];

    function CandidatesController($scope, $stateParams, candidatesService, candidatesApiService, $mdToast) {
        $scope.filters = {};
        candidatesService.getFilters()
            .then(response => $scope.filters = response, reject => {
                $mdToast.show($mdToast.simple()
                    .textContent(`${reject.statusText}`)
                    .position('bottom right')
                    .toastClass('vladloh'));
            });

        $scope.tabs = candidatesService.getTabs();
        $scope.activeTab = $scope.tabs[0];
        $scope.candidates = {};
        $scope.tabs.forEach(tab => $scope.candidates[tab] = []);
        $scope.alreadySelected = $stateParams.selectedFilter || {};
        init();
        $scope.filterConfig = $stateParams.selectedFilter;

        $scope.$on('quickSearch:applyFilter', (event, filterConfig) => {
            init();
            $scope.filterConfig = filterConfig;
            $scope.loadMore();
        });

        $scope.changeTab = tab => {
            init();
            $scope.activeTab = tab;
            $scope.$broadcast('quickSearch:clear');
            $scope.loadMore();

        };

        $scope.loadMore = () => {
            $scope.filterConfig = Object.assign({}, $scope.filterConfig, $scope.params);
            loadWithFilter($scope.activeTab, $scope.filterConfig);
            $scope.params.skip += $scope.params.amount;
        };

        $scope.downloadReport = (filterConfig) => {
            filterConfig.skip = 0;
            $mdToast.show($mdToast.simple()
                .textContent('Processing...')
                .position('bottom right')
                .toastClass('vladloh'));
            candidatesApiService.getReport($scope.activeTab, filterConfig)
                .catch(error => {
                    $mdToast.show($mdToast.simple()
                        .textContent(`${error.statusText}`)
                        .position('bottom right')
                        .toastClass('vladloh'));
                });

        };

        function init() {
            $scope.filterConfig = {};
            $scope.found = ' ';
            $scope.showMore = true;
            $scope.candidates[$scope.activeTab] = null;
            $scope.candidates[$scope.activeTab] = [];
            $scope.isLoading = true;
            $scope.params = {
                skip: 0,
                amount: 10,
            };
            $scope.mode = {
                name: 'candidates',
            }
        }

        function loadWithFilter(tab, filterConfig) {
            $scope.isLoading = true;
            $scope.showMore = false;
            candidatesApiService.getCandidates(tab, filterConfig)
                .then(response => {
                    $scope.candidates[tab] = $scope.candidates[tab].concat(response.candidates);
                    $scope.found = response.amount;
                })
                .catch(error => {
                    $mdToast.show($mdToast.simple()
                        .textContent('An error has occurred')
                        .position('bottom right')
                        .toastClass('vladloh'));
                })
                .finally(() => {
                    $scope.isLoading = false;
                    $scope.showMore = $scope.params.skip <= $scope.found;
                });
        }
    }

})());
((() => {
    'use strict';

    angular.module('app')
        .factory('candidatesService', candidatesService);

    candidatesService.$inject = ['staticService'];

    function candidatesService(staticService) {

        const service = {};

        const tabs = ['Active', 'Archive'];

        let filters = {
            city: [],
            profession: [],
            experience: [],
            englishLevel: [],
            skills: [],
            candidateStatuses: [],
        };

        service.getTabs = () => tabs;

        service.getFilters = () => {
            return staticService.getConfig(filters)
        };

        return service;

    }
})());
((() => {

    'use strict';

    angular.module('app').config(config);

    config.$inject = [
        '$locationProvider',
        '$stateProvider',
        '$urlRouterProvider',
        '$mdThemingProvider'
    ];

    function config($locationProvider, $stateProvider, $urlRouterProvider, $mdThemingProvider) {
        $stateProvider.state({
            name: 'login',
            url: '/login',
            templateUrl: '../login/login.html',
            data: {
                authorizedRoles: ['admin', 'hrm', 'tech']
            }
        }).state({
            name: 'dashboard',
            url: '/dashboard',
            templateUrl: '../admin-dashboard/admin-dashboard.html',
            controller: 'AdminController',
            data: {
                authorizedRoles: ['admin']
            }
        }).state({
            name: 'notifications',
            url: '/notifications',
            templateUrl: '../notifications/notifications.html',
            controller: 'NotificationsController',
            data: {
                authorizedRoles: ['admin', 'hrm', 'tech']
            }
        }).state({
            name: 'error',
            url: '/error',
            templateUrl: '../error/server-error/server-error.html',
            params: {
                error: null,
            },
            controller: 'serverErrorController',
            data: {
                authorizedRoles: ['admin', 'hrm', 'tech']
            }
        });


        $mdThemingProvider.definePalette('ivBlue', {
            '50': 'e3e8eb',
            '100': 'b9c6cd',
            '200': '8aa1ac',
            '300': '5b7b8b',
            '400': '375e72',
            '500': '144259',
            '600': '123c51',
            '700': '0e3348',
            '800': '0b2b3e',
            '900': '061d2e',
            'A100': '68b8ff',
            'A200': '35a0ff',
            'A400': '0288ff',
            'A700': '007be7',
            'contrastDefaultColor': 'light',
            'contrastDarkColors': [
                '50',
                '100',
                '200',
                'A100',
                'A200'
            ],
            'contrastLightColors': [
                '300',
                '400',
                '500',
                '600',
                '700',
                '800',
                '900',
                'A400',
                'A700'
            ]
        });

        $mdThemingProvider.theme('default')
            .primaryPalette('ivBlue');


        $urlRouterProvider.otherwise('/notifications');
        $locationProvider.html5Mode(true);

    }

})());

((() => {

    'use strict';

    angular.module('app').controller('AppController', AppController);

    AppController.$inject = [
        '$scope',
        'loginService',
        'notificationsHubService',
        '$mdToast'
    ];

    function AppController($scope, loginService, notificationsHubService, $mdToast) {

        $scope.isAuthorized = loginService.isAuthorized;

        notificationsHubService.addHandler('displayMessage', notification => {
            $scope.$broadcast('notifications:newNotification', notification);
            $mdToast
                .show($mdToast.notificationPreset()
                    .position('bottom right'));
            let audio = new Audio('vk.mp3');
            audio.play();
        });

        if (loginService.isAuthorized()) {
            notificationsHubService.init();
        }

        if (!loginService.isAuthorized()) {
            notificationsHubService.stop();
        }
    }

})());
((() => {
    'use strict';

    angular.module('app')
        .value('appConstants', {
            apiUrl: '',
            url: '',

            //Add candidate constants
            FOUND_SIMILAR_CANDIDATE_STATUS: 202,
            DEFAULT_CANDIDATE_STATUS: 'Pool',
            DEFAULT_POSITION_STATUS: 'Active',
            LAST_USAGE_MIN_YEAR: 2000,
            LAST_USAGE_MAX_YEAR: (new Date()).getFullYear(),
            SKILL_MIN_RATING: 0,
            SKILL_MAX_RATING: 5,
        });

})());
((() => {
    'use strict';
    angular.module('app')
        .factory('fileWorkerService', fileWorkerService);

    fileWorkerService.$inject = ['$http', '$q', 'appConstants', '$httpParamSerializer'];

    function fileWorkerService($http, $q, appConstants, $httpParamSerializer) {

        const service = {};

        service.getFile = (resourceLink, query) => {

            const deferred = $q.defer();
            query = query || {};
            query = $httpParamSerializer(query);
            const ifrm = document.createElement('iframe');

            ifrm.onload = (resolve) => {
                if (resolve.status !== 200) {
                    deferred.reject({ statusText: 'Not found' });
                }
                deferred.resolve();
                ifrm.remove();
            };
            ifrm.onerror = (error) => {
                deferred.reject(error);
                ifrm.remove();
            };
            ifrm.style.display = 'none';
            ifrm.setAttribute('src', `${appConstants.apiUrl}/${resourceLink}/?${query}`);
            document.body.appendChild(ifrm);
            return deferred.promise;
        };

        service.postResume = (attachment) => {

            return $http({
                url: `${appConstants.apiUrl}/resume/`,
                method: 'POST',
                withCredentials: true,
                data: attachment,
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${localStorage.getItem('token')}`,
                }
            })

        };


        return service;
    }
})());

(() => {
    'use strict';
    angular.module('app')
        .filter('englishLevel', () => {
            return (englishNumber) => {
                const englishSkills = ['Unknown', 'A1', 'A2', 'B1', 'B2', 'C1', 'C2', 'Native'];
                return englishSkills[englishNumber] || 'Unknown'
            }
        })

})();

((() => {
    'use strict';
    angular.module('app')
        .filter('iconFilter', () => iconName => {
            const icons = {
                'Interview': 'mdi-voice',
                'Interview with customer': 'mdi-account-multiple',
                'Candidate declined': 'mdi-account-remove',
                'CV provided': 'mdi-clipboard-text',
                'Closed': 'mdi-close-circle-outline',
                'Waiting for interview with customer': 'mdi-account-switch',
                'Cancelled': 'mdi-close-circle-outline',
                'Candidate approved': 'mdi-account-plus',
                'Active': 'mdi-fire',
                'On hold': 'mdi-pause',
                'Attention': 'mdi-alert',
                'Job offer rejected': 'mdi-close-circle-outline',
                'Job offer': 'mdi-file-plus',
                'In progress': 'mdi-trending-up',
                'Job offer accepted': 'mdi-check-all',
                'Hired': 'mdi-check-all',
                'Pool': 'mdi-pause',
                'Rejected': 'mdi-close'
            };
            if (icons[iconName]) {
                return icons[iconName];
            }
            return 'help';
        })

})());

((() => {
    'use strict';

    angular.module('app')
        .factory('staticService', staticService);

    staticService.$inject = ['$http', '$q', 'appConstants', 'loginService'];

    function staticService($http, $q, appConstants, loginService) {

        const service = {};

        const filterRequest = {
            'city': getCities,
            'profession': getProfessions,
            'experience': getExperience,
            'skills': getSkillsList,
            'candidatestatuses': getCandidateStatuses,
            'positionstatuses': getPositionStatuses,
            'englishlevel': getEnglishLevels,
        };

        function getPositionStatuses() {

            return $http({
                url: `${appConstants.apiUrl}/PositionStatus`,
                method: 'GET',
                headers: {
                    'Authorization': `Bearer ${loginService.getToken()}`,
                }
            })
                .then(response => response.data
                );


        }

        function getEnglishLevels() {
            const deferred = $q.defer();

            const values =
                [
                    {
                        name: '>A1',
                        value: '1'
                    },
                    {
                        name: '>A2',
                        value: '2',
                    },
                    {
                        name: '>B1',
                        value: '3'
                    },
                    {
                        name: '>B2',
                        value: '4'
                    },
                    {
                        name: '>C1',
                        value: '5'
                    },
                    {
                        name: '>C2',
                        value: '6'
                    },
                    {
                        name: 'Native',
                        value: '7'
                    }];

            deferred.resolve(values);

            return deferred.promise;
        }

        function getCandidateStatuses() {

            return $http({
                url: `${appConstants.apiUrl}/Candidatestatus`,
                method: 'GET',
                headers: {
                    'Authorization': `Bearer ${loginService.getToken()}`,
                }
            })
                .then(response => response.data);


        }

        function getCities() {

            return $http({
                url: `${appConstants.apiUrl}/Cities`,
                method: 'GET',
                headers: {
                    'Authorization': `Bearer ${loginService.getToken()}`,
                }
            })
                .then(response => response.data);


        }

        function getSkillsTree() {

            return $http({
                url: `${appConstants.apiUrl}/Skills`,
                method: 'GET',
                headers: {
                    'Authorization': `Bearer ${loginService.getToken()}`,
                }
            })
                .then(response => response.data);


        }

        function getProfessions() {


            return $http({
                url: `${appConstants.apiUrl}/Professions`,
                method: 'GET',
                headers: {
                    'Authorization': `Bearer ${loginService.getToken()}`,
                }
            })
                .then(response => response.data);


        }

        function getSkillsList() {

            return $http({
                url: `${appConstants.apiUrl}/Skills/List`,
                method: 'GET',
                headers: {
                    'Authorization': `Bearer ${loginService.getToken()}`,
                }
            })
                .then(response => response.data);
        }

        function getProjects() {

            return $http({
                url: `${appConstants.apiUrl}/Projects`,
                method: 'GET',
                headers: {
                    'Authorization': `Bearer ${loginService.getToken()}`,
                }
            })
                .then(response => response.data);
        }

        function getExperience() {
            const deferred = $q.defer();

            const exp =
                [
                    {
                        name: '>1 year',
                        value: '1'
                    },
                    {
                        name: '>2 years',
                        value: '2',
                    },
                    {
                        name: '>3 years',
                        value: '3'
                    },
                    {
                        name: '>4 years',
                        value: '4'
                    },
                    {
                        name: '>5 years',
                        value: '5'
                    },
                    {
                        name: '>6 years',
                        value: '6'
                    }];

            deferred.resolve(exp);

            return deferred.promise;
        }

        service.getConfig = config => {

            Object.keys(config).forEach((c) => {
                if (filterRequest[c.toLowerCase()]) {
                    config[c] = filterRequest[c.toLowerCase()]();
                }
            });

            return $q.all(config)
                .then(response => {
                    Object.keys(config).forEach((c) => {
                        config[c] = response[c];
                    });
                    return config;
                });
        };

        service.getPositionStatuses = getPositionStatuses;
        service.getCandidateStatuses = getCandidateStatuses;
        service.getCities = getCities;
        service.getProfessions = getProfessions;
        service.getSkillsList = getSkillsList;
        service.getExperience = getExperience;
        service.getEnglishLevels = getEnglishLevels;
        service.getSkillsTree = getSkillsTree;
        service.getProjects = getProjects;

        return service;

    }

})());
(function () {
    
    'use strict';
    
    Array.prototype.top = function () {
        return this[this.length - 1];
    };
    
    Array.prototype.popOut = function () {
        if (this.length > 1)
            this.pop();
    };
    
    Array.prototype.range = function (begin, end, transform) {
        if (!transform)
            transform = (i) => i;
        const arr = [];
        for (let i = begin; i <= end; i++) {
            arr.push(transform(i));
        }
        return arr;
    };
    
    Array.prototype.indexOfBy = function (obj, equal) {
        let ret = -1;
        this.some((elem, index) => {
            if (equal(obj, elem)) {
                ret = index;
                return true;
            }
        });
        return ret;
    };
    
    Array.prototype.difference = function (array, equal) {
        if (!equal)
            equal = (a, b) => {
                return a === b;
            };
        const res = [];
        this.forEach(elem => {
            const index = array.indexOfBy(elem, equal);
            index === -1 && res.push(elem);
        });
        return res;
    }
    
})();
(() => {

    'use strict';

    angular.module('app')
        .controller('FilterController', FilterController);

    FilterController.$inject = ['$scope', 'staticService'];

    function FilterController($scope, staticService) {

        function init() {
            $scope.control = {};
            $scope.selected = {};
            $scope.alreadySelected = $scope.alreadySelected || {};
            $scope.toggleText = 'Show more options';
            $scope.selected.skills = [];
            $scope.selected = Object.assign({}, $scope.selected, $scope.alreadySelected);
            $scope.showAdvanced = false;
        }

        init();

        function createFilterConfig() {
            const filterConfig = {};
            Object.keys($scope.selected).forEach(item => {
                if ($scope.selected[item]) {
                    filterConfig[item] = $scope.selected[item]
                }
            });
            return filterConfig;
        }

        $scope.applyFilter = () => {
            $scope.$emit('quickSearch:applyFilter', createFilterConfig());
        };

        $scope.skillTreeCallback = staticService.getSkillsTree;

        $scope.goAdvanced = () => {
            $scope.showAdvanced = !$scope.showAdvanced;
            $scope.toggleText = $scope.showAdvanced ? 'Hide more options' : 'Show more options';
        };

        $scope.$on('quickSearch:clear', () => {
            $scope.selected = {};
        });

        $scope.clear = () => {
            $scope.selected = {};
            $scope.applyFilter();
        };

        $scope.fileExport = () => {

            $scope.exportToFile(createFilterConfig());
        }
    }
})();


((() => {

    'use strict';

    angular.module('app')
        .directive('quickFilter', () => ({
            templateUrl: 'filters/filter.html',
            scope: {
                items: '<',
                exportToFile: '=',
                mode: '=',
                alreadySelected: '<',
            },
            controller: 'FilterController',
            link: (scope, element) => {
                element.bind("keydown keypress", event => {
                    if (event.which === 13) {
                        scope.applyFilter();
                        event.preventDefault();
                    }
                });
            }
        }));

})());
(function () {

    'use strict';

    angular.module('app').controller('HeaderController', HeaderController);

    HeaderController.$inject = [
        '$state',
        'loginService',
        'headerService',
        'notificationsHubService',
        '$mdToast',
    ];

    function HeaderController($state, loginService, headerService, notificationsHubService, $mdToast) {

        this.states = headerService.getStates();

        this.state = headerService.getInitState();

        this.changeTab = function (state) {
            this.state = state;
            $state.go(state.name);
        };

        this.getAccess = function () {
            return localStorage.getItem('userRole');
        };

        this.getName = function () {
            return localStorage.getItem('userRole').slice(0, 3);
        };


        this.getEmail = function () {
            return localStorage.getItem('userEmail');
        };


        this.logOut = function () {
            notificationsHubService.stop();
            loginService.logOut();
            $state.go('login');
            $mdToast.hide();
        };
    }

}());
(function () {

    'use strict';

    angular.module('app')
        .factory('headerService', headerService);

    function headerService() {
        const states = [{
            name: 'positions',
            url: '/positions',
            templateUrl: 'positions/positions.html'
        }, {
            name: 'candidates',
            url: '/candidates',
            templateUrl: 'candidates/candidates.html'
        }
        ];

        function getStates() {
            return states;
        }

        function getInitState() {
            return states[1];
        }

        return {
            getStates,
            getInitState,
        };
    }

}());

((() => {
    'use strict';
    angular.module('app')
        .controller('HistoryController', HistoryController);

    HistoryController.$inject = ['$scope', 'id', 'loadCallback'];

    function HistoryController($scope, id, loadCallback) {

        $scope.history = [];

        $scope.query = {
            skip: 0,
            amount: 10
        };

        $scope.showMore = true;

        $scope.loadMore = () => {

            $scope.showMore = false;
            loadCallback(id, $scope.query)
                .then(response => {
                    if (response.length) {
                        $scope.showMore = true;
                    }
                    $scope.query.skip += $scope.query.amount;
                    $scope.history = $scope.history.concat(response);
                })
        };

        $scope.loadMore();

    }

})());

(() => {
    'use strict';
    angular.module('app')
        .filter('historyIcons', () => {
            return (item) => {
                const icons = {
                    'created': 'mdi-plus',
                    'update': 'mdi-pencil',
                };

                let icon = 'mdi-help';

                if (item) {
                    Object.keys(icons).some(key => {
                        if (item.toLowerCase().includes(key)) {
                            icon = icons[key];
                            return true;
                        }
                    })
                }
                return icon;
            }
        })

})();

(function () {

    'use strict';

    angular.module('app')
        .factory('interviewsService', interviewsService);

    interviewsService.$inject = ['$http', 'appConstants'];

    function interviewsService($http, appConstants) {
        const service = {};
        service.sendInterview = function (interview) {
            const request = {
                method: 'POST',
                url: `${appConstants.apiUrl}/interviews`,
                data: interview,
                withCredentials: true,
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${localStorage.getItem('token')}`,
                }
            };
            return $http(request).then(response => {
            });
        };

        service.getInterview = function (id) {
            const request = {
                method: 'GET',
                url: `${appConstants.apiUrl}/interviews/${id}`,
                withCredentials: true,
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${localStorage.getItem('token')}`,
                }
            };
            return $http(request).then(response => {
                return response.data;
            });
        };


        service.putInterview = function (id, interview) {
            const request = {
                method: 'PUT',
                url: `${appConstants.apiUrl}/interviews/${id}`,
                data: interview,
                withCredentials: true,
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${localStorage.getItem('token')}`,
                }
            };
            return $http(request).then(response => {
            });
        };

        service.statusManager = function (typeInterview, feedbackStatus, approvingVerdict) {
            typeInterview = typeInterview || "";
            feedbackStatus = feedbackStatus  || "";
            approvingVerdict = approvingVerdict || "";
            if (feedbackStatus && typeInterview === "ts" && !approvingVerdict) {
                feedbackStatus = feedbackStatus + "ed" + " waiting approving from HR"
            }

            if (approvingVerdict) {
                feedbackStatus = feedbackStatus + "ed";
                approvingVerdict = ", HR " + approvingVerdict +"d";
            }
            
            return "Interview " + typeInterview.toUpperCase().toString() + " " + feedbackStatus.toString() + approvingVerdict;
        };

        service.patchSkills = function (interview, skills) {
            const request = {
                method: 'PATCH',
                url: `${appConstants.apiUrl}/candidates/skills/${interview.candidate.id}`,
                data: { interviewId: interview.id, skills: skills },
                withCredentials: true,
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${localStorage.getItem('token')}`,
                }
            };
            
            
            return $http(request).then(response => {
            });
        };
        return service;

    }
}());
(function () {

    'use strict';


    angular.module('app')
        .controller('LoginController', LoginController);

    LoginController.$inject = [
        '$scope',
        '$state',
        '$mdDialog',
        'loginService',
        'notificationsHubService'
    ];

    function LoginController($scope, $state, $mdDialog, loginService, notificationsHubService) {

        if (loginService.isAuthorized()) {
            $state.go('notifications');
        }

        $scope.submit = (username, password) => {
            loginService.submitLogin(username, password)
                .then(function (response) {
                    loginService.putAccessToken(response.data.access_token);
                    loginService.putUserEmail(username);
                    $state.go('notifications');
                    notificationsHubService.init();
                }, function (response) {
                    $mdDialog.show(
                        $mdDialog.alert()
                            .parent(angular.element(document.querySelector('#popupContainer')))
                            .clickOutsideToClose(true)
                            .title("Sorry!")
                            .textContent(response.data.error_description)
                            .ariaLabel('Alert Dialog Demo')
                            .ok('Ok')
                    );
                });
        }
    }

}());
(function () {

    'use strict';

    angular.module('app')
        .factory('loginService', loginService);

    loginService.$inject = ['$http', 'appConstants', '$state'];

    function loginService($http, appConstants, $state) {
        const service = {};

        service.submitLogin = function (username, password) {
            const request = {
                method: 'POST',
                url: `${appConstants.url}/token`,
                data: `username=${username}&password=${password}&grant_type=password`,
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            };
            return $http(request);
        };


        service.getAccessLevel = function () {
            const request = {
                method: 'GET',
                url: `${appConstants.apiUrl}/users/userinfo/`,
                headers: {
                    'Authorization': `Bearer ${localStorage.getItem('token')}`
                },
            };
            return $http(request);
        };


        service.getHrms = function () {
            const request = {
                method: 'GET',
                url: `${appConstants.apiUrl}/users/hrms`,
                headers: {
                    'Authorization': `Bearer ${localStorage.getItem('token')}`
                },
            };
            return $http(request).then(response => {
                return response.data;
            });
        };

        service.getTechs = function () {
            const request = {
                method: 'GET',
                url: `${appConstants.apiUrl}/users/techs`,
                headers: {
                    'Authorization': `Bearer ${localStorage.getItem('token')}`
                },
            };
            return $http(request).then(response => {
                return response.data;
            });
        };

        service.stateAccess = function (trans) {
            if (service.isAuthorized()) {
                {
                    service.getAccessLevel()
                        .then(function (response) {
                            service.putUserRole(response.data.accessLevel.toLowerCase());
                            let curLevel = service.getUserRole();
                            let flag = true;
                            let curRoles = trans.to().data.authorizedRoles;
                            curRoles.forEach(function (item, curRoles) {
                                if (item === curLevel) {
                                    flag = false;
                                }
                            });
                            if (flag) {
                                $state.go('notifications');
                            }
                        }, function (reject) {
                            if (reject.status === 401) {
                                loginService.logOut();
                                $state.go('login');
                            }
                        });
                }
            }
            else {
                $state.go('login');
            }
        };


        service.putAccessToken = function (token) {
            localStorage.setItem('token', token);
        };

        service.putUserEmail = function (email) {
            localStorage.setItem('userEmail', email);
        };

        service.isAuthorized = function () {
            return localStorage.getItem('token');
        };

        service.getToken = function () {
            return localStorage.getItem('token');
        };

        service.getUserRole = function () {
            return localStorage.getItem('userRole');
        };

        service.putUserRole = function (role) {
            localStorage.setItem('userRole', role);
        };

        service.logOut = function () {
            localStorage.removeItem('token');
            localStorage.removeItem('userRole');
        };

        return service;

    }

}());

(function () {
    'use strict';

    angular.module('app').controller('MobileMenuController', MobileMenuController);

    MobileMenuController.$inject = [
        '$scope',
        '$timeout',
        '$mdSidenav',
        '$log'
    ];


    function MobileMenuController($scope, $timeout, $mdSidenav, $log) {

        const vm = this;
        vm.toggleRight = buildToggler('right');
        vm.isOpenRight = function () {
            return $mdSidenav('right').isOpen();
        };

        function debounce(func, wait, context) {
            let timer;

            return function debounced() {
                let context = $scope,
                    args = Array.prototype.slice.call(arguments);
                $timeout.cancel(timer);
                timer = $timeout(function () {
                    timer = undefined;
                    func.apply(context, args);
                }, wait || 10);
            };
        }

        function buildDelayedToggler(navID) {
            return debounce(function () {
                $mdSidenav(navID)
                    .toggle()
                    .then(function () {
                        $log.debug("toggle " + navID + " is done");
                    });
            }, 200);
        }

        function buildToggler(navID) {
            return function () {
                $mdSidenav(navID)
                    .toggle()
                    .then(function () {
                        $log.debug("toggle " + navID + " is done");

                    });
            };
        }

        vm.closeMenu = function () {
            $mdSidenav('right').close();
        };
    }
}());
(() => {
    'use strict';
    angular.module('app')
        .controller('NotificationsController', NotificationsController);

    NotificationsController.$inject = ['$scope', 'notificationsService', '$window'];

    function NotificationsController($scope, notificationsService, $window) {

        $window.scrollTo(0, 0);

        function init() {

            $scope.notifications = [];

            $scope.showMore = true;

            $scope.isLoading = true;

            $scope.params =
                {
                    skip: 0,
                    amount: 15,
                }
        }

        init();

        $scope.loadMore = () => {
            $scope.isLoading = true;
            $scope.showMore = false;
            notificationsService.getNotifications($scope.params)
                .then(response => {
                    if (response.data.length) {
                        $scope.showMore = true;
                    }
                    $scope.notifications = $scope.notifications.concat(response.data);
                    $scope.params.skip += $scope.params.amount;
                })
                .finally(() => {
                    $scope.isLoading = false;
                });

        };

        $scope.$on('notifications:newNotification', (event, notification) => {
            $scope.notifications.unshift(notification);
        });

    }

})();

((() => {
    'use strict';
    angular.module('app')
        .factory('notificationsHubService', notificationsHubService);

    notificationsHubService.$inject = ['appConstants'];

    function notificationsHubService(appConstants) {

        const service = {};

        let isRunning = false;

        const handlers = [];

        const notificationshub = $.connection.pushNotificationHub;

        $.connection.hub.url = `${appConstants.url}/signalr`;

        service.addHandler = (eventName, handler) => {

            handlers.push({
                eventName, handler
            });
        };
        service.init = () => {
            handlers.forEach(item => {
                notificationshub.client[item.eventName] = (message) => {
                    item.handler(message);
                }
            });

            $.connection.hub.qs = { 'Bearer': `${localStorage.getItem('token')}` };

            $.connection.hub.start({
                jsonp: true,
                transport: ['webSockets', 'longPolling'],
            })
                .done(() => {
                    
                    isRunning = true;
                })
                .fail(a => {
                    
                });
        };

        service.stop = () => {
            if (isRunning) {
                $.connection.hub.stop();
                $.connection.hub.qs = '';
                
                isRunning = false;
            }
        };

        return service;

    }

})());

((() => {
    'use strict';
    angular.module('app')
        .factory('notificationsService', notificationsService);


    notificationsService.$inject = ['$http', 'appConstants'];

    function notificationsService($http, appConstants) {

        const service = {};

        service.getNotifications = (params) => {

            return $http({
                method: 'GET',
                url: `${appConstants.apiUrl}/notifications`,
                headers: {
                    'Authorization': `Bearer ${localStorage.getItem('token')}`,
                },
                params: params,
            })
        };

        service.updateStatus = (notification) => {

            return $http({
                method: 'PUT',
                url: `${appConstants.apiUrl}/Notifications/Read`,
                data: notification,
                withCredentials: true,
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${localStorage.getItem('token')}`,
                }
            })

        };

        return service;

    }

})());
((() => {

    angular.module('app')
        .factory('positionApiService', positionApiService);

    positionApiService.$inject = ['$http', 'appConstants', 'loginService'];

    function positionApiService($http, appConstants, loginService) {

        const service = {};

        service.getPositions = (tab, filter) => {

            filter = filter || {};
            return $http({
                url: `${appConstants.apiUrl}/Positions/${tab}`,
                method: 'GET',
                params: filter,
                headers: {
                    'Authorization': `Bearer ${loginService.getToken()}`,
                }
            })
                .then(response => response.data);
        };

        service.getPositionDetails = (id) => {
            return $http({
                url: `${appConstants.apiUrl}/Positions/${id}`,
                method: 'GET',
                headers: {
                    'Authorization': `Bearer ${loginService.getToken()}`,
                }
            })
                .then(response => {
                    return response.data;
                });

        };

        service.postPosition = (position) => {

            return $http({
                method: 'POST',
                url: `${appConstants.apiUrl}/Positions`,
                data: position,
                withCredentials: true,
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${loginService.getToken()}`,
                }
            });

        };

        service.putPosition = (id, position) => {

            return $http({
                method: 'PUT',
                url: `${appConstants.apiUrl}/Positions/${id}`,
                data: position,
                withCredentials: true,
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${loginService.getToken()}`,
                }
            });

        };

        service.getPositionHistory = (id, query) => {

            return $http({
                method: 'GET',
                url: `${appConstants.apiUrl}/history/position/${id}`,
                params: query,
                headers: {
                    'Authorization': `Bearer ${loginService.getToken()}`,
                }
            })
                .then(response => response.data)
        };

        return service;
    }

})());

(() => {
    'use strict';

    angular.module('app')
        .config(positionConfig);

    positionConfig.$inject = ['$stateProvider'];

    function positionConfig($stateProvider) {

        $stateProvider.state({
            name: 'positions',
            url: '/positions',
            templateUrl: 'positions/positions.html',
            controller: 'PositionsController',
            params: {
                selectedFilter: null,
            },
            data: {
                authorizedRoles: ['admin', 'hrm']
            }
        }).state({
            name: 'position',
            url: '/positions/:id',
            templateUrl: 'positions/position-details/position-details.html',
            controller: 'PositionDetailsController',
            params: {
                id: null,
                selectedFilter: null,
            },
            data: {
                authorizedRoles: ['admin', 'hrm']
            }
        });
    }

})();
(() => {

    'use strict';

    angular.module('app')
        .controller('PositionsController', PositionsController);

    PositionsController
        .$inject = ['$scope', '$stateParams', 'positionsService', 'positionApiService', '$mdToast'];

    function PositionsController($scope, $stateParams, positionsService, positionApiService, $mdToast) {
        $scope.positions = {};
        $scope.filters = {};
        $scope.alreadySelected = $stateParams.selectedFilter || {};

        $scope.filters = positionsService.getFilters()
            .then(response => $scope.filters = response, reject => {
                $mdToast.show($mdToast.simple()
                    .textContent(`${reject.statusText}`)
                    .position('bottom right')
                    .toastClass('vladloh'));
            });

        $scope.tabs = positionsService.getTabs();

        $scope.activeTab = $scope.tabs[0];

        $scope.tabs.forEach(tab => $scope.positions[tab] = []);
        init();
        $scope.filterConfig = $stateParams.selectedFilter;

        $scope.$on('quickSearch:applyFilter', (event, filterConfig) => {
            init();
            $scope.filterConfig = filterConfig;
            $scope.loadMore();
        });

        $scope.changeTab = (tab) => {
            init();
            $scope.activeTab = tab;
            $scope.$broadcast('quickSearch:clear');
            $scope.loadMore()

        };

        $scope.loadMore = () => {
            $scope.filterConfig = Object.assign({}, $scope.filterConfig, $scope.params);
            loadWithFilter($scope.activeTab, $scope.filterConfig);
            $scope.params.skip += $scope.params.amount;
        };

        function init() {
            $scope.params = {
                skip: 0,
                amount: 10,
            };
            $scope.filterConfig = {};
            $scope.found = ' ';
            $scope.positions[$scope.activeTab] = null;
            $scope.positions[$scope.activeTab] = [];
            $scope.showMore = true;
            $scope.isLoading = true;
            $scope.mode = {
                name: 'positions',
            }
        }

        function loadWithFilter(tab, filterConfig) {
            $scope.showMore = false;
            $scope.isLoading = true;
            positionApiService.getPositions(tab, filterConfig)
                .then(response => {
                    $scope.positions[tab] = $scope.positions[tab].concat(response.positions);
                    $scope.found = response.amount;
                })
                .catch(error => {
                    $mdToast.show($mdToast.simple()
                        .textContent('An error has occurred')
                        .position('bottom right')
                        .toastClass('vladloh'));
                })
                .finally(() => {
                    $scope.isLoading = false;
                    $scope.showMore = $scope.params.skip <= $scope.found;
                });
        }

    }

})();

((() => {

    'use strict';

    angular.module('app')
        .factory('positionsService', positionsService);

    positionsService.$inject = ['staticService'];

    function positionsService(staticService) {

        const service = {};

        const tabs = ['Active', 'Archive'];

        const filters = {
            city: [],
            profession: [],
            experience: [],
            positionStatuses: [],
        };

        service.getTabs = () => tabs;

        service.getFilters = () => {
            return staticService.getConfig(filters);
        };

        return service;

    }

})());


((() => {
    'use strict';
    angular.module('app')
        .directive('stars', () => ({
            scope: {
                count: '<'
            },
            template: '<i ng-repeat="i in getNumber(count) track by $index" class="mdi mdi-star mdi-24px color-yellow"></i>',
            controller: $scope => {
                $scope.getNumber = num => new Array(num)
            }
        }))
})());

(function () {
    
    'use strict';
    
    angular.module('app').controller('SimilarCandidatesController', similarCandidatesController);
    
    similarCandidatesController.$inject = [
        '$scope',
        'candidates',
        'candidatesApiService'
    ];
    
    function similarCandidatesController($scope, candidates, candidatesApiService) {
        
        $scope.candidates = candidates.map(candidate => {
            candidate.linkText = 'Show';
            candidate.isOpened = false;
            return candidate;
        });
        
        $scope.showCandidateDetails = function (candidate) {
            const newMode = !candidate.isOpened;
            candidate.isOpened = newMode;
            candidate.linkText = newMode ? 'Hide' : 'Show';
        };
        
        $scope.openCandidateDetails = function (candidate) {
            if (!candidate.details) {
                candidate.sendingRequest = true;
                candidatesApiService.getCandidateDetails(candidate.id)
                    .then(response => {
                        candidate.sendingRequest = false;
                        candidate.details = response;
                        $scope.showCandidateDetails(candidate);
                    }, error => {
                        candidate.sendingRequest = false;
                        
                    });
            } else
                $scope.showCandidateDetails(candidate);
        }
        
    }
    
})();
(function () {
    
    'use strict';
    
    angular.module('app').controller('SkillsTreeController', skillsTreeController);
    
    skillsTreeController.$inject = [
        '$scope',
        '$compile',
        '$templateCache'
    ];
    
    function skillsTreeController($scope, $compile, $templateCache) {
        
        const treeFuncPromise = $scope.skillsTreeRequest();
        
        function stringComparator(a, b) {
            if (a.toUpperCase() > b.toUpperCase())
                return 1;
            if (a.toUpperCase() < b.toUpperCase())
                return -1;
            return 0;
        }
        
        function nameComparator(a, b) {
            return stringComparator(a.name, b.name);
        }
        
        function sortChildren(node) {
            
            if (node.children) {
                
                const listHaveChildren = [], listHaveNoChildren = [];
                
                node.children.forEach(elem => elem.children ? listHaveChildren.push(elem) : listHaveNoChildren.push(elem));
                listHaveChildren.sort(nameComparator);
                listHaveNoChildren.sort(nameComparator);
                node.children = listHaveChildren.concat(listHaveNoChildren);
                
                node.children.forEach(sortChildren);
                
            }
            
        }
        
        function sortTree() {
            sortChildren($scope.skillsTree);
        }
        
        function treeTraversal(node, func) {
            node.children && node.children.forEach(elem => {
                func(elem);
                treeTraversal(elem, func)
            });
        }
        
        function toRootTraversal(node, func) {
            if (node.parentId) {
                const parent = $scope.nameObject[node.parentId];
                func(parent);
                toRootTraversal(parent, func);
            }
        }
    
        function countBackDropParams() {
            const visArea = angDoc.documentElement;
        
            return {
                width: visArea.clientWidth,
                height: visArea.clientHeight,
            };
        }
    
        function countTreeParams(e) {
            const rect = e.target.getBoundingClientRect();
            const x = parseInt(rect.left) - 10, y = parseInt(rect.top) - 10;
        
            return {
                x: x,
                y: y,
            };
        }
    
        function createBackDrop() {
        
            //Creating a tree backdrop
            const templateBackDrop = angular.element($templateCache.get('templateBackDrop'))[0];
            const params = countBackDropParams();
            templateBackDrop.style.width = `${params.width}px`;
            templateBackDrop.style.height = `${params.height}px`;
            const backDropElement = $compile(templateBackDrop.outerHTML)($scope)[0];
            angDoc.body.insertBefore(backDropElement, angDoc.body.firstElementChild);
        
        }
    
        function createScroll() {
        
            angDoc.body.style.top = `${-pageYOffset}px`;
            angDoc.body.style.position = 'fixed';
            angDoc.body.style.overflowY = 'scroll';
        
        }
    
        function createTree(e) {
        
            //Creating a tree dropdown
            let templateDiv;
            if ($scope.multiselect)
                templateDiv = angular.element($templateCache.get('templateMultiTree'))[0];
            else
                templateDiv = angular.element($templateCache.get('templateTree'))[0];
            const params = countTreeParams(e);
            templateDiv.style.left = `${params.x}px`;
            templateDiv.style.top = `${params.y}px`;
            // templateDiv.style.visibility = 'hidden';
            //Check if it dont cross the window
            const treeElement = $compile(templateDiv.outerHTML)($scope)[0];
            angDoc.body.appendChild(treeElement);
        
        }
    
        function removeBackDrop() {
            const backdrop = angDoc.getElementsByClassName('tree-backdrop')[0];
            angDoc.body.removeChild(backdrop);
        }
    
        function removeScroll() {
            angDoc.body.style.position = '';
            const yoffset = -parseInt(angDoc.body.style.top);
            angDoc.body.style.top = '';
            angDoc.body.style.overflowY = '';
            window.scrollTo(0, yoffset);
        }
    
        function removeTree() {
            const treeElement = angDoc.getElementsByClassName('tree-content-container')[0];
            angDoc.body.removeChild(treeElement);
        }
    
        function changeStyleIfNeeded() {
            const treeElement = angDoc.getElementsByClassName('content-overlap')[0];
            if (treeElement) {
                const rect = treeElement.getBoundingClientRect();
                const ledgeBot = rect.bottom - angDoc.documentElement.clientHeight;
                const ledgeTop = rect.top;
                //Supposing only one time moving
                if (ledgeBot > 0)
                    treeElement.style.top = `${-ledgeBot - 10}px`;
                if (ledgeTop < 0)
                    treeElement.style.top = `${-ledgeTop + 10}px`;
                // treeElement.style.visibility = 'visible';
            
            }
        }
        
        function initTree() {
            
            $scope.openTree = function (e) {
                createScroll();
                createBackDrop();
                createTree(e);
                setTimeout(changeStyleIfNeeded, 0);
            };
            
            $scope.closeTree = function () {
                removeBackDrop();
                removeScroll();
                removeTree();
            };
    
            $scope.ac = {
                path: [
                    $scope.skillsTree,
                ],
            };
            
            $scope.goToBranch = function (node, ac) {
                ac.path.push(node);
                setTimeout(changeStyleIfNeeded, 0);
            };
            
            $scope.returnToBranch = function (node, index, ac) {
                ac.path = ac.path.slice(0, index + 1);
                setTimeout(changeStyleIfNeeded, 0);
            };
            
            function addSkillNamesByIds(skills) {
                skills.forEach(elem => {
                    if ($scope.chosenSkillName !== '')
                        $scope.chosenSkillName += ', ';
                    $scope.chosenSkillName += $scope.nameObject[elem].name;
                });
            }
    
            function addSkillNames(names) {
                names.forEach(elem => {
                    if ($scope.chosenSkillName !== '')
                        $scope.chosenSkillName += ', ';
                    $scope.chosenSkillName += elem;
                });
            }
            
            function removeSkillNames(skills) {
                const skillNames = skills.map(skill => $scope.nameObject[skill].name);
                const chosenNames = $scope.chosenSkillName.split(', ');
                skillNames.forEach(elem => chosenNames.splice(chosenNames.indexOf(elem), 1));
                $scope.chosenSkillName = '';
                addSkillNames(chosenNames);
            }
            
            if ($scope.multiselect) {
                
                $scope.chosenSkillName = '';
                
                $scope.clear = function () {
        
                    $scope.chosenSkillName = '';
                    treeTraversal($scope.skillsTree, (node) => {
                        node.chosen.value = false;
                        node.indeterminate = false;
                        node.disabled = false;
                    });
        
                };
    
                $scope.selectNode = function (node) {
                    node.chosen.value = true;
                    toRootTraversal(node, elem => elem.indeterminate = true);
                    treeTraversal(node, elem => {
                        if (elem.chosen.value) {
                            $scope.unselectSkill(elem.id);
                            $scope.unselectNode(elem);
                        }
                        elem.disabled = true;
                    });
                };
    
                $scope.unselectNode = function (node) {
                    node.chosen.value = false;
                    toRootTraversal(node, elem => elem.indeterminate = false);
                    treeTraversal(node, elem => elem.disabled = false);
                };
    
                $scope.selectSkill = function (skill) {
                    $scope.chosenSkill.push(skill);
                };
    
                $scope.unselectSkill = function (skill) {
                    $scope.chosenSkill.splice($scope.chosenSkill.indexOf(skill), 1);
                };
    
                $scope.pressNode = function (node) {
                    if (node.chosen.value) {
                        $scope.unselectSkill(node.id);
                        $scope.unselectNode(node);
                    } else {
                        $scope.selectSkill(node.id);
                        $scope.selectNode(node);
                    }
                };
                
                treeTraversal($scope.skillsTree, node => {
                    node.chosen = { value: false };
                });
                
                if (!$scope.chosenSkill || $scope.chosenSkill.length === 0)
                    $scope.chosenSkill = [];
                else
                    treeTraversal($scope.skillsTree, (node) => {
                        const index = $scope.chosenSkill.findIndex(elem => node.id === elem);
                        if (index !== -1) {
                            $scope.selectNode(node);
                            addSkillNamesByIds([node.id]);
                        }
                    });
                
                $scope.$watchCollection('chosenSkill', function (newVal, oldVal) {
                    if (!newVal || newVal.length === 0) {
                        $scope.chosenSkill = [];
                        $scope.chosenSkillName = '';
                        $scope.clear();
                    } else {
                        if (oldVal) {
                            removeSkillNames(oldVal.difference(newVal));
                            addSkillNamesByIds(newVal.difference(oldVal));
                        }
                    }
                });
                
            } else {
                
                if ($scope.chosenSkill && $scope.chosenSkill !== '') {
                    $scope.chosenSkillName = $scope.nameObject[$scope.chosenSkill].name;
                    const newNode = $scope.nameObject[$scope.chosenSkill];
                    toRootTraversal(newNode, elem => {
                        $scope.ac.path.splice(1, 0, elem);
                    });
                }
                
                $scope.selectSkill = function (node) {
                    $scope.chosenSkill = node.id;
                    $scope.closeTree();
                };
                
                $scope.$watch('chosenSkill', function (newVal, oldVal) {
                    if (!newVal || newVal === '') {
                        $scope.chosenSkill = '';
                        $scope.chosenSkillName = '';
                    } else
                        $scope.chosenSkillName = $scope.nameObject[newVal].name;
                    
                });
                
            }
            
        }
        
        const angDoc = angular.element(document)[0];
        
        treeFuncPromise().then(response => {
            
            $scope.skillsTree = response;
            $scope.skillsTree.id = '0';
            
            $scope.nameObject = {};
            
            treeTraversal($scope.skillsTree, node => {
                $scope.nameObject[node.id] = node;
            });
            
            sortTree($scope.skillsTree);
            
            initTree();
            
        }, error => {
            
        });
        
    }
    
})();
(function () {
    
    'use strict';
    
    angular.module('app').directive('skillsTree', () => ({
        
        templateUrl: 'add-candidate/skills-tree/skills-tree.html',
        scope: {
            skillsTreeRequest: '&',
            chosenSkill: '=',
            multiselect: '<',
            control: '=',
            required: '<',
            mdNoFloat: '<'
        },
        controller: 'SkillsTreeController'
    }));
    
})();

(function () {
    
    'use strict';
    
    angular.module('app').directive('email', () => ({
        
        restrict: 'A',
        require: 'ngModel',
        link: function (scope, element, attr, ngModel) {
            
            const emailRegEx = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            ngModel.$validators.email = function (value) {
                return !value || value.match(emailRegEx);
            };
            
        }
        
    }));
    
})();

(function () {
    
    'use strict';
    
    angular.module('app').directive('language', () => ({
        
        restrict: 'A',
        require: 'ngModel',
        link: function (scope, element, attr, ngModel) {
            
            const lang = attr.language;
            let regExp;
            switch (lang) {
                case 'rus':
                    regExp = /[^а-яА-Я]/;
                    break;
                case 'eng':
                    regExp = /[^a-zA-Z]/;
                    break;
            }
            ngModel.$validators.language = function(value) {
                return !value || !value.match(regExp);
            };
            
        }
        
    }));
    
})();
(function () {
    
    'use strict';
    
    angular.module('app').directive('maxLength', () => ({

        restrict: 'A',
        require: 'ngModel',
        link: function (scope, element, attr, ngModel) {
            
            const max = parseInt(attr.maxLength);
            ngModel.$validators.maxLength = function (value) {
                return !value || value.length <= max;
            };

        }

    }));

    angular.module('app').directive('minLength', () => ({

        restrict: 'A',
        require: 'ngModel',
        link: function (scope, element, attr, ngModel) {

            const min = parseInt(attr.minLength);
            ngModel.$validators.minLength = function (value) {
                return !value || value.length >= min;
            };

        }

    }));
    
})();
(function () {
    
    'use strict';
    
    angular.module('app').directive('phone', () => ({
        
        restrict: 'A',
        require: 'ngModel',
        link: function (scope, element, attr, ngModel) {
            
            const phoneRegEx = /^\+[0-9]{3,15}$/;
            ngModel.$validators.phone = function (value) {
                return !value || value.match(phoneRegEx);
            };
            
        }
        
    }));
    
})();

((() => {
    'use strict';
    angular.module('app')
        .directive('candidateCard', () => ({
            templateUrl: 'candidates/candidate-card/candidate-card.html',
            scope: {
                candidate: '<',
                filterConfig: '<',
            },
            controller: CandidateCardController,
        }));


    CandidateCardController.$inject = ['$scope', '$state'];

    function CandidateCardController($scope, $state) {

        $scope.candidateDetails = id => {

            const selectedFilter = $scope.filterConfig;
            $state.go('candidate', { id, selectedFilter });

        }
    }
})());

/* eslint-disable angular/controller-as */
((() => {
    'use strict';
    angular.module('app')
        .controller('CandidateDetailsController', CandidateDetailsController);

    CandidateDetailsController.$inject = [
        '$scope', '$state', '$stateParams',
        'candidatesApiService', '$mdToast', '$mdDialog', '$document'];

    function CandidateDetailsController($scope, $state, $stateParams, candidatesApiService, $mdToast, $mdDialog, $document) {

        $scope.isLoading = true;
        $scope.limit = 3;


        candidatesApiService.getCandidateDetails($stateParams.id)
            .then(response => {
                $scope.candidate = response;
            }, error => {
                $state.go('error', { error }, { location: false })
            })
            .finally(() => {
                $scope.isLoading = false;
            });

        $scope.goBack = () => {
            $state.go('candidates', { selectedFilter: $stateParams.selectedFilter });
        };

        $scope.addCandidate = () => {
            $state.go('add-candidate');
        };

        $scope.getResume = (id) => {
            candidatesApiService.getCandidateResume(id)
                .then(() => {
                    $mdToast.show($mdToast.simple()
                        .textContent('Success!')
                        .position('bottom right')
                        .toastClass('vladloh'));
                })
                .catch(error => {
                    $mdToast.show($mdToast.simple()
                        .textContent(`${error.statusText}`)
                        .position('bottom right')
                        .toastClass('vladloh'));
                });
        };

        $scope.addInterview = () => {
            $mdDialog.show({
                controller: 'InterviewsAddController',
                templateUrl: 'interviews/interviews-add/interviews-add.html',
                parent: angular.element($document.body),
                clickOutsideToClose: true,
                locals: {
                    candidate: $scope.candidate
                }
            });
        };

        $scope.getInterview = (id) => {

            $mdDialog.show({
                controller: 'InterviewsDetailsController',
                templateUrl: 'interviews/interviews-details/interviews-details.html',
                parent: angular.element($document.body),
                clickOutsideToClose: true,
                locals: {
                    id
                }
            })
        };

        $scope.getMoreInterviews = () => {

            if ($scope.limit === 3) {
                $scope.limit = $scope.candidate.interviews.length;
            }
        };

        $scope.getHistory = (id) => {
            $mdDialog.show({
                parent: angular.element($document.body),
                templateUrl: 'history/history.html',
                clickOutsideToClose: true,
                controller: 'HistoryController',
                locals: {
                    id,
                    loadCallback: candidatesApiService.getCandidateHistory
                }
            });

        };

        $scope.editCandidate = (id) => {

            $state.go('edit-candidate', { id })

        };

    }
})());

((() => {

    'use strict';
    angular.module('app')
        .controller('serverErrorController', serverErrorController);

    serverErrorController.$inject = ['$scope', '$stateParams'];

    function serverErrorController($scope, $stateParams) {

        $scope.error = $stateParams.error;
    }

})());

((() => {
    'use strict';
    angular.module('app')
        .controller('InterviewsAddController', InterviewsAddController);

    InterviewsAddController.$inject = [
        '$scope',
        '$mdDialog',
        'interviewsService',
        'loginService',
        'positionApiService',
        'candidate'
    ];

    function InterviewsAddController($scope, $mdDialog, interviewsService, loginService, positionApiService, candidate) {
        $scope.interview = {
            type: "ts",
            candidate: {
                id: ""
            },
            header: "",
            interviewerEmail: "",
            hrmEmail: localStorage.getItem('userEmail'),
            englishLevel: 0,
            date: "",
            skills: [],
            position: "",
            description: ""
        };
        $scope.date="";
        $scope.time="";

        $scope.selected = [];
        $scope.candidate = candidate;
        $scope.skills = candidate.skills;
        $scope.interview.candidate.id = candidate.id;


        $scope.toggle = function (item, list) {
            let idx = list.indexOf(item);
            if (idx > -1) {
                list.splice(idx, 1);
            }
            else {
                list.push(item);
            }
        };

        $scope.exists = function (item, list) {
            return list.indexOf(item) > -1;
        };

        $scope.isIndeterminate = function () {
            return ($scope.selected.length !== 0 &&
            $scope.selected.length !== $scope.skills.length);
        };

        $scope.isChecked = function () {
            return $scope.selected.length === $scope.skills.length;
        };

        $scope.toggleAll = function () {
            if ($scope.selected.length === $scope.skills.length) {
                $scope.selected = [];
            } else if ($scope.selected.length === 0 || $scope.selected.length > 0) {
                $scope.selected = $scope.skills.slice(0);
            }
        };

        loginService.getHrms().then(response => {
            $scope.hrmsSpec = response;
        });

        loginService.getTechs().then(response => {
            $scope.techsSpec = response;
        });

        positionApiService.getPositions("active").then(response => {
            $scope.positions = response.positions;
        });

        $scope.hide = function () {
            $mdDialog.hide();
        };


        $scope.cancel = function () {
            $mdDialog.cancel();
        };

        $scope.answer = function (answer) {
            $mdDialog.hide(answer);
        };


        $scope.sendInterview = function () {
            $scope.interview.skills = $scope.selected.map(item => {
                return {
                    skillId: item.id,
                    defultRating: item.rating,
                    confRating: 0,
                    wasConfirmed: false,
                    experience: item.experience,
                    isPrimary: item.isPrimary,
                    skillName: item.name
                }
            });
            const dateDate = new Date(`${$scope.date}`);
            const dateTime = new Date(`${$scope.time}`);
            dateDate.setHours(dateTime.getHours());
            dateDate.setMinutes(dateTime.getMinutes());

            $scope.interview.date = dateDate;
            $scope.interview.status = interviewsService.statusManager($scope.interview.type);
            interviewsService.sendInterview($scope.interview);
            $mdDialog.cancel();
        }
    }
})());

((() => {
    'use strict';
    angular.module('app')
        .controller('InterviewsApproveController', InterviewsApproveController);

    InterviewsApproveController.$inject = [
        '$scope',
        '$state',
        '$mdDialog',
        'interviewsService',
        'id'
    ];

    function InterviewsApproveController($scope, $state, $mdDialog, interviewsService, id) {
        interviewsService.getInterview(id).then(response => {
            $scope.interview = response;
            if ($scope.interview.status.indexOf("waiting")!==-1){
                $scope.wasConfirmed = false;
            } else{
                $scope.wasConfirmed = true;
            }
        })
            .catch(error => {
                $state.go('error', {error: error});
            })
            .finally(() => {
                $scope.isLoading = false;
            });

        $scope.approvingVerdict="Approve";



        $scope.putInterview = function () {
                $scope.interview.status =interviewsService.statusManager(
                $scope.interview.type,
                $scope.interview.feedbackMessage,
                $scope.approvingVerdict
            );




            if($scope.approvingVerdict == "Approve") {
                $scope.skillsForPatch = $scope.interview.skills.map(item => {
                    return {
                        id: item.skillId,
                        rating: item.confRating,
                        wasConfirmed: true,
                        experience: item.experience,
                        isPrimary: item.isPrimary,
                        name: item.skillName,
                    }
                });
                interviewsService.patchSkills($scope.interview, $scope.skillsForPatch);
            }
            interviewsService.putInterview($scope.interview.id, $scope.interview);
            $mdDialog.cancel();
        };

        $scope.candidateDetails = function () {
            $state.go('candidate', {id: $scope.interview.candidate.id});
            $mdDialog.cancel();
        };

        $scope.getAccess = function () {
            return localStorage.getItem('userRole');
        };

    }
})());
((() => {
    'use strict';
    angular.module('app')
        .controller('InterviewsDetailsController', InterviewsDetailsController);

    InterviewsDetailsController.$inject = [
        '$scope',
        '$mdToast',
        'interviewsService',
        'id'
    ];

    function InterviewsDetailsController($scope, $mdToast, interviewsService, id) {
        interviewsService.getInterview(id).then(response => {
            $scope.interview = response;
        })
            .catch(error => {
                $mdToast.show($mdToast.simple()
                    .textContent(`${error.statusText}`)
                    .position('bottom right')
                    .toastClass('vladloh'));
            })
            .finally(() => {
                $scope.isLoading = false;
            });

        $scope.candidateDetails = function () {
            $state.go('candidate', {id: $scope.interview.candidate.id});
            $mdDialog.cancel();
        }
    }
})());

((() => {

    'use strict';
    angular.module('app')
        .controller('InterviewsFeedbackController', InterviewsFeedbackController);

    InterviewsFeedbackController.$inject = [
        '$scope',
        '$state',
        '$mdDialog',
        '$mdToast',
        'interviewsService',
        'staticService',
        'id',
    ];

    function InterviewsFeedbackController($scope, $state, $mdDialog, $mdToast, interviewsService, staticService, id) {
        interviewsService.getInterview(id).then(response => {
            $scope.interview = response;
            $scope.interview.feedbackMessage = "Approve";
            if (response.type === "ts") {
                $scope.interview.feedbackMessage = "Recommend";
            }

            $scope.wasConfirmed = false;
            if($scope.interview.status.length > 15){
                $scope.wasConfirmed = true;
            }

        }).catch(error => {
            $mdToast.show($mdToast.simple()
                .textContent(`${error.statusText}`)
                .position('bottom right')
                .toastClass('vladloh'));
        })
            .finally(() => {
                $scope.isLoading = false;
            })
        ;

        $scope.skillsTreeRequest = staticService.getSkillsTree;

        $scope.printSkills = function () {
            
        };


        $scope.englishRanks = [
            { key: "disabled", val: 0 },
            { key: "A1", val: 1 },
            { key: "A2", val: 2 },
            { key: "B1", val: 3 },
            { key: "B2", val: 4 },
            { key: "C1", val: 5 },
            { key: "C2", val: 6 },
            { key: "Native", val: 7 }
        ];

        $scope.skillRanks = [
            { key: "0", val: 0 },
            { key: '1', val: 1 },
            { key: '2', val: 2 },
            { key: '3', val: 3 },
            { key: '4', val: 4 },
            { key: '5', val: 5 }
        ];

        $scope.putInterview = function () {

            $scope.interview.status = interviewsService.statusManager(
                $scope.interview.type,
                $scope.interview.feedbackMessage
            );

            interviewsService.putInterview($scope.interview.id, $scope.interview);
            $mdDialog.cancel();
        };


        $scope.candidateDetails = function () {
            $state.go('candidate', { id: $scope.interview.candidate.id });
            $mdDialog.cancel();
        };

        $scope.getAccess = function () {
            return localStorage.getItem('userRole');
        };
    }
})());
((() => {
    'use strict';
    angular.module('app')
        .controller('InterviewsPreviewController', InterviewsPreviewController);

    InterviewsPreviewController.$inject = [
        '$scope',
        '$state',
        '$mdDialog',
        'interviewsService',
        'id'
    ];

    function InterviewsPreviewController($scope, $state, $mdDialog, interviewsService, id) {
        interviewsService.getInterview(id).then(response => {
            $scope.interview = response;
        })
            .catch(error => {
                $mdToast.show($mdToast.simple()
                    .textContent(`${error.statusText}`)
                    .position('bottom right')
                    .toastClass('vladloh'));
            })
            .finally(() => {
                $scope.isLoading = false;
            });

        $scope.candidateDetails = function () {
            $state.go('candidate', {id: $scope.interview.candidate.id});
            $mdDialog.cancel();
        };

        $scope.getAccess = function () {
            return localStorage.getItem('userRole');
        };
    }
})());

((() => {
    'use strict';
    angular.module('app')
        .controller('NotificationCardController', NotificationCardController);

    NotificationCardController.$inject = ['$scope', '$state', '$mdDialog', 'notificationsService', 'loginService', '$document'];

    function NotificationCardController($scope, $state, $mdDialog, notificationsService, loginService, $document) {


        const actionMap = {
            'feedback': id => {
                $mdDialog.show({
                    controller: 'InterviewsFeedbackController',
                    templateUrl: 'interviews/interviews-feedback/interviews-feedback.html',
                    parent: angular.element($document.body),
                    clickOutsideToClose: true,
                    locals: {
                        id: id
                    }
                })
            },
            'interview': id => {
                $mdDialog.show({
                    controller: 'InterviewsPreviewController',
                    templateUrl: 'interviews/interviews-preview/interviews-preview.html',
                    parent: angular.element($document.body),
                    clickOutsideToClose: true,
                    locals: {
                        id: id
                    }
                })
            },
            'approve': id => {
                $mdDialog.show({
                    controller: 'InterviewsApproveController',
                    templateUrl: 'interviews/interviews-approve/interviews-approve.html',
                    parent: angular.element($document.body),
                    clickOutsideToClose: true,
                    locals: {
                        id: id
                    }
                })
            }
        };

        $scope.changeStatus = () => {

            if (!$scope.notification.isRead) {

                $scope.notification.isRead = true;

                notificationsService.updateStatus($scope.notification)
            }
        };

        $scope.openNotification = () => {

            const routeType = $scope.notification.routeType;
            const routeId = $scope.notification.routeId;
            if (actionMap[$scope.notification.type]) {
                actionMap[$scope.notification.type](routeId)
            }
            else {
                $state.go(routeType, { id: routeId })
            }
        };

    }
})());
((() => {
    'use strict';
    angular.module('app')
        .directive('notificationCard', () => ({
            templateUrl: 'notifications/notifications-card/notification-card.html',
            scope: {
                notification: '=',
            },
            controller: 'NotificationCardController',
        }))
})());

((() => {
    'use strict';
    angular.module('app')
        .filter('notifyIcons', () => iconName => {
            const icons = {
                'interview': 'mdi-account-circle color-red',
                'info': 'mdi-information color-indigo',
                'feedback': 'mdi-file-document color-green',
                'attention': 'mdi-alert color-red',
                'approve': 'mdi-check color-green'
            };
            if (iconName) {
                return icons[iconName.toLowerCase()] || 'help';
            }
            return 'help';
        })

})());

((() => {

    'use strict';

    angular.module('app')
        .config(config);

    config.$inject = ['$mdToastProvider'];

    function config($mdToastProvider) {
        $mdToastProvider.addPreset('notificationPreset', {
            options: () => ({
                templateUrl: 'notifications/toaster/notification-toast.html',
                controller: 'NotificationToastController',
                hideDelay: 4000,
                toastClass: 'vladloh'
            })
        })
        ;
    }

})());

((() => {

    'use strict';
    angular.module('app')
        .controller('NotificationToastController', NotificationToastController);


    NotificationToastController.$inject = ['$scope', '$state', '$mdToast'];

    function NotificationToastController($scope, $state, $mdToast) {

        $scope.openMoreInfo = () => {

            $state.go('notifications');
            $mdToast
                .hide()
        };

        $scope.closeToast = () => {
            $mdToast
                .hide()
        }


    }

})());
(() => {

    'use strict';

    angular.module('app')
        .directive('positionCard', () => ({
            templateUrl: 'positions/position-card/position-card.html',
            scope: {
                position: '<',
                filterConfig: '<',
            },
            controller: PositionCardController,
        }));

    PositionCardController.$inject = ['$scope', '$state'];

    function PositionCardController($scope, $state) {

        $scope.positionDetails = id => {
            const selectedFilter = $scope.filterConfig;
            $state.go('position', { id, selectedFilter });

        }

    }
})();

(() => {

    'use strict';

    angular.module('app')
        .controller('PositionDetailsController', PositionDetailsController);

    PositionDetailsController.$inject = ['$scope', '$state', '$stateParams', 'positionApiService', 'candidatesApiService', '$mdDialog', '$document'];

    function PositionDetailsController($scope, $state, $stateParams, positionApiService, candidatesApiService, $mdDialog, $document) {
        const topFilter = {};
        $scope.isLoading = true;
        $scope.limit = 3;
        $scope.topThreeCandidates = [];
        $scope.topLoading = true;

        $scope.goBack = () => {
            $state.go('positions', { selectedFilter: $stateParams.selectedFilter });
        };

        positionApiService.getPositionDetails($stateParams.id)
            .then(response => {
                    $scope.position = response;
                    $scope.isLoading = false;
                },
                error => {
                    $state.go('error', { error }, { location: false })
                })
            .then(() => {
                topFilter.amount = 3;
                topFilter.skills = $scope.position.skills.map(skill => skill.id);
                topFilter.cities = [$scope.position.city.id];
                topFilter.professions = [$scope.position.profession.id];
                return candidatesApiService.getCandidates('active', topFilter)
            })
            .then(response => $scope.topThreeCandidates = response.candidates)
            .finally(() => {
                $scope.topLoading = false;
            });

        $scope.addPosition = () => {
            $state.go('add-position');
        };

        $scope.getHistory = (id) => {

            $mdDialog.show({
                parent: angular.element($document.body),
                templateUrl: 'history/history.html',
                clickOutsideToClose: true,
                controller: 'HistoryController',
                locals: {
                    id,
                    loadCallback: positionApiService.getPositionHistory
                }
            })
        };

        $scope.editPosition = (id) => {
            $state.go('edit-position', { id })
        };

        $scope.getCandidatesForPosition = () => {

            topFilter.amount = 10;

            $state.go('candidates', { selectedFilter: topFilter })
        }
    }

})();

