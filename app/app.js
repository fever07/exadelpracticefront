(() => {
    'use strict';

    angular.module('app', ['ui.router', 'ngMaterial', 'infinite-scroll', 'ngAnimate', 'ngMessages'])
        .run(appRun);
    appRun.$inject = ['$transitions', 'loginService', 'appConstants'];
    function appRun($transitions, loginService, appConstants) {
        const el = document.getElementsByTagName('html');
        appConstants.apiUrl = el[0].getAttribute('data-apiUrl');
        appConstants.url = el[0].getAttribute('data-url');
        $transitions.onStart({}, function (trans) {
            loginService.stateAccess(trans);
        });
    }
})();