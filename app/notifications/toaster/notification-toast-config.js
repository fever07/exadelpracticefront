((() => {

    'use strict';

    angular.module('app')
        .config(config);

    config.$inject = ['$mdToastProvider'];

    function config($mdToastProvider) {
        $mdToastProvider.addPreset('notificationPreset', {
            options: () => ({
                templateUrl: 'notifications/toaster/notification-toast.html',
                controller: 'NotificationToastController',
                hideDelay: 4000,
                toastClass: 'vladloh'
            })
        })
        ;
    }

})());
