((() => {

    'use strict';
    angular.module('app')
        .controller('NotificationToastController', NotificationToastController);


    NotificationToastController.$inject = ['$scope', '$state', '$mdToast'];

    function NotificationToastController($scope, $state, $mdToast) {

        $scope.openMoreInfo = () => {

            $state.go('notifications');
            $mdToast
                .hide()
        };

        $scope.closeToast = () => {
            $mdToast
                .hide()
        }


    }

})());