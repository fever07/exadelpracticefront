(() => {
    'use strict';
    angular.module('app')
        .controller('NotificationsController', NotificationsController);

    NotificationsController.$inject = ['$scope', 'notificationsService', '$window'];

    function NotificationsController($scope, notificationsService, $window) {

        $window.scrollTo(0, 0);

        function init() {

            $scope.notifications = [];

            $scope.showMore = true;

            $scope.isLoading = true;

            $scope.params =
                {
                    skip: 0,
                    amount: 15,
                }
        }

        init();

        $scope.loadMore = () => {
            $scope.isLoading = true;
            $scope.showMore = false;
            notificationsService.getNotifications($scope.params)
                .then(response => {
                    if (response.data.length) {
                        $scope.showMore = true;
                    }
                    $scope.notifications = $scope.notifications.concat(response.data);
                    $scope.params.skip += $scope.params.amount;
                })
                .finally(() => {
                    $scope.isLoading = false;
                });

        };

        $scope.$on('notifications:newNotification', (event, notification) => {
            $scope.notifications.unshift(notification);
        });

    }

})();
