((() => {
    'use strict';
    angular.module('app')
        .factory('notificationsHubService', notificationsHubService);

    notificationsHubService.$inject = ['appConstants'];

    function notificationsHubService(appConstants) {

        const service = {};

        let isRunning = false;

        const handlers = [];

        const notificationshub = $.connection.pushNotificationHub;

        $.connection.hub.url = `${appConstants.url}/signalr`;

        service.addHandler = (eventName, handler) => {

            handlers.push({
                eventName, handler
            });
        };
        service.init = () => {
            handlers.forEach(item => {
                notificationshub.client[item.eventName] = (message) => {
                    item.handler(message);
                }
            });

            $.connection.hub.qs = { 'Bearer': `${localStorage.getItem('token')}` };

            $.connection.hub.start({
                jsonp: true,
                transport: ['webSockets', 'longPolling'],
            })
                .done(() => {
                    console.log('Connected to HUB');
                    isRunning = true;
                })
                .fail(a => {
                    console.log('Not connected' + a);
                });
        };

        service.stop = () => {
            if (isRunning) {
                $.connection.hub.stop();
                $.connection.hub.qs = '';
                console.log('HUB stopped');
                isRunning = false;
            }
        };

        return service;

    }

})());
