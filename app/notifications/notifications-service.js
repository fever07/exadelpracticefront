((() => {
    'use strict';
    angular.module('app')
        .factory('notificationsService', notificationsService);


    notificationsService.$inject = ['$http', 'appConstants'];

    function notificationsService($http, appConstants) {

        const service = {};

        service.getNotifications = (params) => {

            return $http({
                method: 'GET',
                url: `${appConstants.apiUrl}/notifications`,
                headers: {
                    'Authorization': `Bearer ${localStorage.getItem('token')}`,
                },
                params: params,
            })
        };

        service.updateStatus = (notification) => {

            return $http({
                method: 'PUT',
                url: `${appConstants.apiUrl}/Notifications/Read`,
                data: notification,
                withCredentials: true,
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${localStorage.getItem('token')}`,
                }
            })

        };

        return service;

    }

})());