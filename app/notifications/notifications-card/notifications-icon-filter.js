((() => {
    'use strict';
    angular.module('app')
        .filter('notifyIcons', () => iconName => {
            const icons = {
                'interview': 'mdi-account-circle color-red',
                'info': 'mdi-information color-indigo',
                'feedback': 'mdi-file-document color-green',
                'attention': 'mdi-alert color-red',
                'approve': 'mdi-check color-green'
            };
            if (iconName) {
                return icons[iconName.toLowerCase()] || 'help';
            }
            return 'help';
        })

})());
