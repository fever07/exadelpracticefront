((() => {
    'use strict';
    angular.module('app')
        .directive('notificationCard', () => ({
            templateUrl: 'notifications/notifications-card/notification-card.html',
            scope: {
                notification: '=',
            },
            controller: 'NotificationCardController',
        }))
})());
