((() => {
    'use strict';
    angular.module('app')
        .controller('NotificationCardController', NotificationCardController);

    NotificationCardController.$inject = ['$scope', '$state', '$mdDialog', 'notificationsService', 'loginService', '$document'];

    function NotificationCardController($scope, $state, $mdDialog, notificationsService, loginService, $document) {


        const actionMap = {
            'feedback': id => {
                $mdDialog.show({
                    controller: 'InterviewsFeedbackController',
                    templateUrl: 'interviews/interviews-feedback/interviews-feedback.html',
                    parent: angular.element($document.body),
                    clickOutsideToClose: true,
                    locals: {
                        id: id
                    }
                })
            },
            'interview': id => {
                $mdDialog.show({
                    controller: 'InterviewsPreviewController',
                    templateUrl: 'interviews/interviews-preview/interviews-preview.html',
                    parent: angular.element($document.body),
                    clickOutsideToClose: true,
                    locals: {
                        id: id
                    }
                })
            },
            'approve': id => {
                $mdDialog.show({
                    controller: 'InterviewsApproveController',
                    templateUrl: 'interviews/interviews-approve/interviews-approve.html',
                    parent: angular.element($document.body),
                    clickOutsideToClose: true,
                    locals: {
                        id: id
                    }
                })
            }
        };

        $scope.changeStatus = () => {

            if (!$scope.notification.isRead) {

                $scope.notification.isRead = true;

                notificationsService.updateStatus($scope.notification)
            }
        };

        $scope.openNotification = () => {

            const routeType = $scope.notification.routeType;
            const routeId = $scope.notification.routeId;
            if (actionMap[$scope.notification.type]) {
                actionMap[$scope.notification.type](routeId)
            }
            else {
                $state.go(routeType, { id: routeId })
            }
        };

    }
})());