const gulp = require("gulp"),
    util = require("gulp-util"),
    sass = require("gulp-sass"),
    autoprefixer = require('gulp-autoprefixer'),
    minifycss = require('gulp-minify-css'),
    rename = require('gulp-rename'),
    log = util.log,
    concat = require('gulp-concat'),
    ngAnnotate = require('gulp-ng-annotate'),
    uglify = require('gulp-uglify-es').default,
    gulp_remove_logging = require("gulp-remove-logging");

gulp.task("scssCompile", ['concatScss'], function () {
    log("Generate CSS files " + (new Date()).toString());
    gulp.src('./dist/stylesInterviewer.scss')
        .pipe(sass({ style: 'expanded' }))
        .pipe(gulp.dest("app"))
        .pipe(rename({ suffix: '.min' }))
        .pipe(minifycss())
        .pipe(gulp.dest("./app/"));
});

gulp.task('concatScss', function () {
    return gulp.src(['!app/bower_components/**/*.scss',
        './app/**/*.scss'])
        .pipe(concat('stylesInterviewer.scss'))
        .pipe(gulp.dest('./dist/'));
});

gulp.task('compress', ['remove_logging'], function () {
    return gulp.src('./dist/bundle.js')
        .pipe(ngAnnotate())
        .pipe(rename({ suffix: '.min' }))
        .pipe(uglify({ mangle: true }))
        .pipe(gulp.dest('./dist/'));
});

gulp.task("remove_logging", ['concatJs'], function () {
    return gulp.src('./dist/bundle.js')
        .pipe(gulp_remove_logging({}))
        .pipe(gulp.dest('./dist/'));
});

gulp.task('concatJs', function () {
    return gulp.src(['!app/bower_components/**/*.js',
        './app/**/*.js'])
        .pipe(concat('bundle.js'))
        .pipe(gulp.dest('./dist/'));
});

gulp.task('watcher', function () {
    gulp.watch('./app/**/*.scss', ["scssCompile"]);
    gulp.watch('./app/**/*.js', ["compress"]);
});