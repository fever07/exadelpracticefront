const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const path = require('path');

const mustacheExpress = require('mustache-express');

app.engine('html', mustacheExpress());
app.set('view engine', 'mustache');

app.set('views', __dirname + '/app');

const data = {
    url: 'https://interviewer.azurewebsites.net',
    hubUrl: "https://interviewer.azurewebsites.net/signalr",
    apiUrl: "https://interviewer.azurewebsites.net/api",
    signalrHubs: "https://interviewer.azurewebsites.net/signalr/hubs",
};


app.set('port', (process.env.PORT || 3000));
app.use(express.static('app', { redirect: false }));
app.use(bodyParser.json());


app.get('/*', (req, res) => {

    res.render('index.html', data);

});


app.listen(app.get('port'), () => console.log('Site started on port ', app.get('port')));